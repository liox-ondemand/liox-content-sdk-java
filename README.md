[![gitflow-avh](https://img.shields.io/badge/gitflow--avh-used-brightgreen.svg)](https://github.com/petervanderdoes/gitflow-avh)
[![Codeship](https://img.shields.io/codeship/63591280-7c43-0134-ae6e-164a30546007.svg)](https://app.codeship.com/projects/181034)
[![Codacy](https://img.shields.io/codacy/grade/23c0dff434b44489b32422e930598d46.svg)](https://www.codacy.com/app/spilth/lionbridge-java)
[![Maven Central](https://img.shields.io/maven-central/v/com.lionbridge.content.sdk/liox-content-sdk-java.svg)](http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22liox-content-sdk-java%22)

# Lionbridge Content SDK for Java

This library is a client SDK which provides a Service wrapper and Object-Oriented mechanism for communicating with the Lionbridge onDemand API Connector web service.

## End-User Documentation

To use the Lionbridge Content SDK in your own Java application, please see the [End-User Documentation](http://developers.lionbridge.com/content/jsdk-docs/)

## Prerequisites

- [Git](https://git-scm.com)
- [JDK 1.7+](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3.X](https://maven.apache.org)
- [GitFlow AVH](https://github.com/petervanderdoes/gitflow-avh)
- [Lionbridge onDemand Developer Sandbox API Key](https://developer-sandbox.liondemand.com/user/profile/)

### Mac

On a Mac you can install the prerequisites using [Homebrew](http://brew.sh/):

```bash
$ brew install git
$ brew install maven
$ brew install git-flow-avh
$ brew cask install java
```

## Getting Started

To check out and run the tests for the project, do the following:

```bash
$ git clone git@bitbucket.org:liox-ondemand/liox-content-sdk-java.git
$ cd liox-content-sdk-java
$ export LIOX_API_ACCESS_KEY_ID=<your-sandbox-api-key>
$ export LIOX_API_SECRET_KEY=<your-sandbox-api-secret>
$ export LIOX_API_ENDPOINT=https://developer-sandbox.liondemand.com/api
$ export LIOX_API_DEFAULT_CURRENCY=USD
$ mvn
```

## Git Flow

We use GitFlow for development of the JSDK, with the default configuration.

To get started you'll want to initialize GitFlow and make sure you have the latest `develop` branch.

```bash
$ git flow init --defaults --force
$ git co develop
$ git pull origin develop
```

### Features

All functionality and bug fixes should be worked on as features:

```bash
$ git flow feature start foo
# writes a test
# make the test pass
# refactor your code
# ensure tests still pass
$ git add <your-changes>
$ git commit -m "your descriptive commit message"
$ git push origin head
# make sure code passes on CI
```

Once you've ensured that your code passes on CI it's time to finish your feature and push up the merged `develop` branch.

```bash
$ git flow feature finish foo
$ git flow feature publish foo
$ git co develop
$ git push origin develop
```

You're now ready to work on your next feature or create a release!

### Releases

To create a release of the Lionbridge Content SDK, do the following:

```bash
$ git flow release start X.Y.Z
$ mvn versions:set -DnewVersion=X.Y.Z
$ rm pom.xml.versionsBackup
$ git add pom.xml
$ git commit -m "Releasing X.Y.Z"
$ git flow release publish X.Y.Z
$ git flow finish X.Y.Z
```

## Deploying

### Snapshots

Whenever a `develop` branch build passes on CI, a snapshot is automatically deployed to <https://oss.sonatype.org/>. 

Snapshots for the onDemand client can be found at <https://oss.sonatype.org/content/repositories/snapshots/com/lionbridge/content/sdk/liox-content-sdk-java/>.

### Releases

To deploy a release version of the onDemand Client to Maven Central requires a few steps:

#### Sonatype Account

- [Sign up for a Sonatype account](https://issues.sonatype.org/secure/Signup!default.jspa)
- Request to be added to the Lionbridge group

#### GPG

```bash
$ brew install gpg
$ gpg --gen-key
# Key Type: 1 - RSA and RSA (default)
# Key Size: 2048
# Key Duration: 1y
# Enter your real name
# Enter your email address
# No Comment
# Enter a passphrase
# Grab the ID for your public key after the `2048R/`
$ gpg --keyserver hkp://pool.sks-keyservers.net --send-keys YOUR_PUBLIC_ID
```

#### Maven Settings

Now you need to add your Sonatype login and GPG information to your Maven settings file (`~/.m2/settings.xml`):

```xml
<settings>
  <profiles>
    <profile>
      <id>default</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <properties>
        <gpg.executable>gpg</gpg.executable>
        <gpg.passphrase>YOUR_GPG_PASSPHRASE</gpg.passphrase>
      </properties>
    </profile>
  </profiles>
  <servers>
    <server>
      <id>ossrh</id>
      <username>YOUR_SONATYPE_USERNAME</username>
      <password>YOUR_SONATYPE_PASSWORD</password>
    </server>
  </servers>
</settings>
```

#### Package, Sign and Deploy

Check out master and deploy using the `release` profile.

```bash
$ git co master
$ mvn deploy -P release
```

#### Publish to Maven Central

Now it's time to inspect the staged release and push it to Maven Central

- Log in to <https://oss.sonatype.org>
- Under `Build Promotion` in the sidebar click on `Staging Repositories`
- In the upper right, search for `lionbridge`
- Click on the release
- Click on `Close`
- Click on the release
- Click on `Release`
- Now wait for the release to show up under <http://repo1.maven.org/maven2/com/lionbridge/content/sdk/liox-content-sdk-java/>

## License

Provided under the MIT License.

## Trademarks

The trademarks, logos, and service marks (collectively “Trademarks”) appearing on any Lionbridge website (and in particular, included in the Lionbridge onDemand API Connector) are the property of Lionbridge Technologies, Inc. and other parties. Nothing contained on the Lionbridge website should be construed as granting any license or right to use any Trademark without the prior written permission of the party that owns the Trademark. In particular, Lionbridge onDemand™, onDemand™, the Lionbridge onDemand logo, Lionbridge™ and the Lionbridge™ logo are trademarks of Lionbridge Technologies, Inc. Lionbridge® and the Lionbridge® logo are registered trademarks of Lionbridge Technologies, Inc. in the United States and several non-US countries.
