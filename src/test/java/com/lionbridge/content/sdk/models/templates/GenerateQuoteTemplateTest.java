package com.lionbridge.content.sdk.models.templates;

import com.lionbridge.content.sdk.models.NotificationSubscription;
import com.lionbridge.content.sdk.models.Project;
import com.lionbridge.content.sdk.models.TranslationOptions;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class GenerateQuoteTemplateTest {

    private GenerateQuoteTemplate quoteTemplate;

    private TranslationOptions translationOptions = new TranslationOptions();
    private List<Project> projects = new ArrayList<>();
    private Project project = new Project();

    @Before
    public void before() {
        project.setProjectId(42);
        projects.add(project);
    }

    @Test
    public void toXmlString_withProject() {
        quoteTemplate = new GenerateQuoteTemplate(
                translationOptions,
                projects
        );

        assertThat(quoteTemplate.toXmlString(), equalTo(
                "<GenerateQuote>" +
                        "<Projects>" +
                        "<Project><ProjectID>42</ProjectID></Project>" +
                        "</Projects>" +
                        "</GenerateQuote>"
                )
        );
    }

    @Test
    public void toXmlString_withProjectAndNotifications() {
        List<NotificationSubscription> notificationSubscriptions = new ArrayList<>();
        NotificationSubscription notificationSubscription = new NotificationSubscription();
        notificationSubscription.setEventName("quote-ready");
        notificationSubscription.setEndpoint("http://foo.com/bar");
        notificationSubscriptions.add(notificationSubscription);

        NotificationSubscription notificationSubscription2 = new NotificationSubscription();
        notificationSubscription2.setEventName("quote-ready");
        notificationSubscription2.setEndpoint("bar@foo.com");
        notificationSubscriptions.add(notificationSubscription2);

        quoteTemplate = new GenerateQuoteTemplate(
                translationOptions,
                projects,
                notificationSubscriptions
        );

        assertThat(quoteTemplate.toXmlString(), equalTo(
                "<GenerateQuote>" +
                        "<Projects>" +
                        "<Project><ProjectID>42</ProjectID></Project>" +
                        "</Projects>" +
                        "<NotificationSubscriptions>" +
                        "<NotificationSubscription>" +
                        "<EventName>quote-ready</EventName>" +
                        "<Endpoint>http://foo.com/bar</Endpoint>" +
                        "</NotificationSubscription>" +
                        "<NotificationSubscription>" +
                        "<EventName>quote-ready</EventName>" +
                        "<Endpoint>mailto:bar@foo.com</Endpoint>" +
                        "</NotificationSubscription>" +
                        "</NotificationSubscriptions>" +
                        "</GenerateQuote>"
                )
        );
    }
}