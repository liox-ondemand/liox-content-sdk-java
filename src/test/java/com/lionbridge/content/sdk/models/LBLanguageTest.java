package com.lionbridge.content.sdk.models;

import org.junit.Test;

import static java.lang.Boolean.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LBLanguageTest {
    @Test
    public void setLanguageCode_withExtraSpace_trimsSpace() {
        TargetLanguage a = new TargetLanguage();
        a.setLanguageCode(" en-us ");

        assertThat(a.getLanguageCode(), equalTo("en-us"));
    }

    @Test
    public void equals_whenLanguageCodesMatch_returnsTrue() {
        TargetLanguage a = new TargetLanguage("en-us");
        TargetLanguage b = new TargetLanguage("EN-US");

        assertThat(a.equals(b), is(TRUE));
    }

    @Test
    public void equals_whenLanguageCodesDoNotMatch_returnsFalse() {
        TargetLanguage a = new TargetLanguage("en-us");
        TargetLanguage b = new TargetLanguage("de-de");

        assertThat(a.equals(b), is(FALSE));
    }
}