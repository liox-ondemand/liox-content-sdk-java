package com.lionbridge.content.sdk.models;

import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class TranslationOptionsTest {
    @Test
    public void toXmlString() throws URISyntaxException {
        TranslationOptions translationOptions = new TranslationOptions();
        translationOptions.setCurrency("USD");
        translationOptions.setSpecialInstructions("Foo");
        translationOptions.setServiceId(42);
        translationOptions.setSourceLanguage(new SourceLanguage("en-us"));
        translationOptions.setTargetLanguages(new ArrayList<>(Arrays.asList(
                new TargetLanguage("de-de"),
                new TargetLanguage("fr-fr")
        )));

        String optionsXml = translationOptions.toXmlString();

        assertThat(optionsXml, containsString("<Currency>USD</Currency>"));
        assertThat(optionsXml, containsString("<ServiceID>42</ServiceID>"));
        assertThat(optionsXml, containsString("<SourceLanguage><LanguageCode>en-us</LanguageCode></SourceLanguage>"));
        assertThat(optionsXml, containsString("<TargetLanguages><TargetLanguage><LanguageCode>de-de</LanguageCode></TargetLanguage><TargetLanguage><LanguageCode>fr-fr</LanguageCode></TargetLanguage></TargetLanguages>"));
        assertThat(optionsXml, containsString("<SpecialInstructions>Foo</SpecialInstructions>"));
    }
}