package com.lionbridge.content.sdk.models.templates;

import com.lionbridge.content.sdk.models.TranslationOptions;
import org.junit.Test;

import java.util.ArrayList;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class AddProjectTemplateTest {
    private AddProjectTemplate addProjectTemplate;

    private TranslationOptions translationOptions = new TranslationOptions();

    @Test
    public void toXmlString_withNoFiles() {
        addProjectTemplate = new AddProjectTemplate(
                "Foo",
                translationOptions,
                null,
                null,
                null
        );

        assertThat(addProjectTemplate.toXmlString(), equalTo(
                "<AddProject>" +
                        "<ProjectName>Foo</ProjectName>" +
                        "<TranslationOptions>" +
                        "<Currency>null</Currency>" +
                        "<TargetLanguages></TargetLanguages>" +
                        "</TranslationOptions>" +
                        "</AddProject>"));
    }

    @Test
    public void toXmlString_withFiles() {
        addProjectTemplate = new AddProjectTemplate(
                "Foo",
                translationOptions,
                new ArrayList<>(asList("123", "456")),
                null,
                null
        );

        assertThat(addProjectTemplate.toXmlString(), equalTo(
                "<AddProject>" +
                        "<ProjectName>Foo</ProjectName>" +
                        "<TranslationOptions>" +
                        "<Currency>null</Currency>" +
                        "<TargetLanguages></TargetLanguages>" +
                        "</TranslationOptions>" +
                        "<Files>" +
                        "<File><AssetID>123</AssetID></File>" +
                        "<File><AssetID>456</AssetID></File>" +
                        "</Files>" +
                        "</AddProject>"));
    }

    @Test
    public void toXmlString_withFilesAndReferenceFiles() {
        addProjectTemplate = new AddProjectTemplate(
                "Foo",
                translationOptions,
                new ArrayList<>(asList("123", "456")),
                new ArrayList<>(asList("321", "654")),
                null
        );

        assertThat(addProjectTemplate.toXmlString(), equalTo(
                "<AddProject>" +
                        "<ProjectName>Foo</ProjectName>" +
                        "<TranslationOptions>" +
                        "<Currency>null</Currency>" +
                        "<TargetLanguages></TargetLanguages>" +
                        "</TranslationOptions>" +
                        "<Files>" +
                        "<File><AssetID>123</AssetID></File>" +
                        "<File><AssetID>456</AssetID></File>" +
                        "</Files>" +
                        "<ReferenceFiles>" +
                        "<ReferenceFile><AssetID>321</AssetID></ReferenceFile>" +
                        "<ReferenceFile><AssetID>654</AssetID></ReferenceFile>" +
                        "</ReferenceFiles>" +
                        "</AddProject>"
        ));
    }
}