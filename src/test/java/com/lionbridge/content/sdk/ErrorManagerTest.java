package com.lionbridge.content.sdk;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ErrorManagerTest {
    private ErrorManager errorManager;

    @Before
    public void setUp() throws Exception {
        errorManager = new ErrorManager();
        errorManager.setStatus("fail");
        errorManager.setErrorCode("404");
    }

    @Test
    public void getters() {
        assertThat(errorManager.getStatus(), equalTo("fail"));
        assertThat(errorManager.getErrorCode(), equalTo("404"));
        assertThat(errorManager.getErrors(), is(nullValue()));
    }

    @Test
    public void toXmlString_withErrors() {
        addErrors();

        assertThat(errorManager.toXmlString(), equalTo("\n<errors>\n\t<error>\n\t\t<reasonCode>42</reasonCode>\n\t\t<simpleMessage>simple message</simpleMessage>\n\t\t<detailedMessage>detailed messages</detailedMessage>\n\t</error>\n</errors>"));
    }

    private void addErrors() {
        LBError error = new LBError();
        error.setDetailedMessage("detailed messages");
        error.setReasonCode(42);
        error.setSimpleMessage("simple message");

        List<LBError> errors = new ArrayList<>();
        errors.add(error);

        errorManager.setErrors(errors);
    }

    @Test
    public void toXmlString_withoutErrors() {
        assertThat(errorManager.toXmlString(), equalTo("\n<errors>\n</errors>"));
    }

    @Test
    public void generateException_createsContentAPIExceptionWithMessage() {
        addErrors();

        ContentAPIException contentAPIException = errorManager.generateException();

        assertThat(contentAPIException.getMessage(), containsString("simple message: detailed messages"));
    }
}