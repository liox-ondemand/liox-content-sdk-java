package com.lionbridge.content.sdk.utilities;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class XmlUtilsTest {
    @Test
    public void appendXmlTag_returnsBuilderWithAppendedXmlTag() {
        StringBuilder builder = new StringBuilder();
        XmlUtils.appendXmlTag(builder, "foo", "bar");

        assertThat(builder.toString(), equalTo("<foo>bar</foo>"));
    }

}