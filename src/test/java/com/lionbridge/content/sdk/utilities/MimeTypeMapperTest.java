package com.lionbridge.content.sdk.utilities;

import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MimeTypeMapperTest {
    private MimeTypeMapper detector = new MimeTypeMapper();

    @Test
    public void getMimeType_whenFileExtensionKnown_returnsCorrectMimeType() {
        File file = new File("./pom.xml");
        assertThat(detector.getMimeType(file), equalTo("application/xml"));
    }

    @Test
    public void getMimeType_whenFileExtensionUnknown_returnsNull() {
        File file = new File("./README.md");
        assertThat(detector.getMimeType(file), equalTo(null));
    }

    @Test
    public void getMimeType_whenFilenameWithComplexName_returnsCorrectMimeType() {
        assertThat(detector.getMimeType("foo/bar.backup.xml"), equalTo("application/xml"));
    }

    @Test
    public void getMimeType_whenFilenameExtensionKnown_returnsCorrectMimeType() {
        Map<String, String> testData = new HashMap<>();

        testData.put("json", "application/json");
        testData.put("resjson", "application/json");
        testData.put("pdf", "application/pdf");
        testData.put("epub", "application/epub+zip");
        testData.put("rtf", "application/rtf");
        testData.put("idml", "application/xml");
        testData.put("inx", "application/xml");
        testData.put("resw", "application/xml");
        testData.put("resx", "application/xml");
        testData.put("xlf", "application/xml");
        testData.put("xliff", "application/xml");
        testData.put("xml", "application/xml");
        testData.put("mif", "application/vnd.mif");
        testData.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        testData.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        testData.put("doc", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        testData.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        testData.put("odf", "application/vnd.oasis.opendocument.text");
        testData.put("odt", "application/vnd.oasis.opendocument.text");
        testData.put("ods", "application/vnd.oasis.opendocument.text");
        testData.put("ott", "application/vnd.oasis.opendocument.text");
        testData.put("zip", "application/zip");
        testData.put("jpg", "image/jpeg");
        testData.put("jpeg", "image/jpeg");
        testData.put("png", "image/png");
        testData.put("psd", "image/vnd.adobe.photoshop");
        testData.put("csv", "text/csv");
        testData.put("htm", "text/html");
        testData.put("html", "text/html");
        testData.put("ini", "text/plain");
        testData.put("po", "text/plain");
        testData.put("pot", "text/plain");
        testData.put("properties", "text/plain");
        testData.put("srt", "text/plain");
        testData.put("strings", "text/plain");
        testData.put("txt", "text/plain");
        testData.put("vtt", "text/plain");
        testData.put("yml", "text/yaml");
        testData.put("yaml", "text/yaml");
        testData.put("mp4", "video/mp4");
        testData.put("mov", "video/quicktime");
        testData.put("flv", "video/x-flv");
        testData.put("m4v", "video/x-m4v");
        testData.put("wmv", "video/x-ms-wmv");
        testData.put("mpeg", "video/mpeg");

        for (String extension : testData.keySet()) {
            String filename = format("test.%s", extension);

            assertThat(
                    detector.getMimeType(filename),
                    equalTo(testData.get(extension))
            );
        }
    }
}