package com.lionbridge.content.sdk;

import com.lionbridge.content.sdk.definitions.FileStatus;
import com.lionbridge.content.sdk.models.*;
import com.lionbridge.content.sdk.models.Locale;
import com.lionbridge.content.sdk.utilities.ZipUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestName;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.valueOf;
import static java.lang.System.getenv;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class ContentAPITest {
    @Rule
    public TestName testName = new TestName();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final int PSEUDO_TRANSLATION_SERVICE_ID = 146;
    private static final int MACHINE_TRANSLATION_SERVICE_ID = 144;
    private static final int PSEUDO_WPML_SERVICE_ID = 272;
    private static final String AUTOMATICALLY_APPROVED_PO_NUMBER = "123456";

    private ContentAPI api;
    private List<NotificationSubscription> addQuoteNotifications;
    private List<NotificationSubscription> addProjectNotifications;

    @Before
    public void before() throws ContentAPIException {
        api = new ContentAPI(
                getenv("LIOX_API_ACCESS_KEY_ID"),
                getenv("LIOX_API_SECRET_KEY"),
                getenv("LIOX_API_ENDPOINT"),
                getenv("LIOX_API_DEFAULT_CURRENCY")
        );

        addQuoteNotifications = new ArrayList<>();
        NotificationSubscription notificationSubscription = new NotificationSubscription();
        notificationSubscription.setEventName("quote-ready");
        notificationSubscription.setEndpoint("http://foo.com/bar");
        addQuoteNotifications.add(notificationSubscription);

        addProjectNotifications = new ArrayList<>();
        NotificationSubscription notificationSubscription2 = new NotificationSubscription();
        notificationSubscription2.setEventName("project-complete");
        notificationSubscription2.setEndpoint("http://foo.com/bar");
        addQuoteNotifications.add(notificationSubscription2);

    }

    @Test
    public void isValid_withValidEndpoint_returnsTrue() throws ContentAPIException {
        assertThat(api.isValid(), is(TRUE));
    }

    @Test
    public void isValid_withInvalidEndpoint_returnsFalse() throws ContentAPIException {
        api = new ContentAPI(
                getenv("LIOX_API_ACCESS_KEY_ID"),
                getenv("LIOX_API_SECRET_KEY"),
                "https://developer-sandbox.liondemand.com/WRONG",
                getenv("LIOX_API_DEFAULT_CURRENCY")
        );

        assertThat(api.isValid(), is(FALSE));
    }

    @Test
    public void journeyTest() throws ContentAPIException, InterruptedException {
        String projectName = getTimeStamp();

        LBFile addedFile = api.addFile("text/plain", new File("src/test/resources/ContentAPI_addFile_test.txt"), "en-us");
        assertThat(addedFile.getAssetId(), notNullValue());

        LBFile addedReferenceFile = api.addFile("text/plain", new File("src/test/resources/ContentAPI_addFile_test2.txt"), "en-us");
        assertThat(addedReferenceFile.getAssetId(), notNullValue());

        LBFile retrievedFileDetails = api.getFileDetails(valueOf(addedFile.getAssetId()));
        assertThat(retrievedFileDetails.getAssetId(), equalTo(addedFile.getAssetId()));

        Project addedProject = api.addProject(
                projectName,
                createTranslationOptions(),
                createFileAssetIds(addedFile),
                createFileAssetIds(addedReferenceFile),
                addProjectNotifications
        );
        assertThat(addedProject.getName(), equalTo(projectName));

        Project retrievedProject = api.getProject(valueOf(addedProject.getProjectId()));
        assertThat(retrievedProject.getProjectId(), equalTo(addedProject.getProjectId()));

        Quote addedQuote = api.addQuote(
                createTranslationOptions(),
                createProjectList(addedProject),
                addQuoteNotifications
        );
        assertThat(addedQuote.getQuoteId(), notNullValue());

        Quote retrievedQuote = api.getQuote(valueOf(addedQuote.getQuoteId()));
        assertThat(retrievedQuote.getQuoteId(), equalTo(addedQuote.getQuoteId()));

        while(retrievedQuote.getStatus().equals("Calculating")) {
            Thread.sleep(1000);
            retrievedQuote = api.getQuote(valueOf(addedQuote.getQuoteId()));
        }
        assertThat(retrievedQuote.getStatus(), equalTo("Pending"));

        List<Quote> retrievedQuotes = api.listQuotes();
        assertThat(retrievedQuotes.size(), greaterThan(0));

        QuoteAuthorization quoteAuthorization = api.authorizeQuote(
                valueOf(addedQuote.getQuoteId()),
                AUTOMATICALLY_APPROVED_PO_NUMBER
        );
        assertThat(quoteAuthorization.getStatus(), equalTo("Authorized"));
    }

    @Test
    public void constructor_whenCurrencyNotSupplied_setsCurrencyToNull() throws ContentAPIException {
        api = new ContentAPI(
                getenv("LIOX_API_ACCESS_KEY_ID"),
                getenv("LIOX_API_SECRET_KEY"),
                getenv("LIOX_API_ENDPOINT")
        );

        assertThat(api.getDefaultCurrency(), is(nullValue()));
    }

    @Test
    public void constructor_withInvalidUrl_throwsException() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        api = new ContentAPI(
                getenv("LIOX_API_ACCESS_KEY_ID"),
                getenv("LIOX_API_SECRET_KEY"),
                getenv("htp://w"),
                getenv("LIOX_API_DEFAULT_CURRENCY")
        );
    }

    @Test
    public void isValid_whenValid_returnsTrue() throws ContentAPIException {
        assertThat(api.isValid(), is(TRUE));
    }

    @Test
    public void getProject_whenNullProjectId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getProject(null);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get project info for null ID."));

            throw e;
        }
    }

    @Test
    public void getProject_whenBlankProjectId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getProject(" ");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get project info for blank ID."));

            throw e;
        }
    }

    @Test
    public void getQuote_whenNullQuoteId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getQuote(null);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get quote info for null ID."));

            throw e;
        }
    }

    @Test
    public void getQuote_whenBlankQuoteId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getQuote(" ");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get quote info for blank ID."));

            throw e;
        }
    }

    @Test
    public void getFile_whenNullFileId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getFile(null);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get file info for null ID."));

            throw e;
        }
    }

    @Test
    public void getFile_whenBlankFileId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getFile(" ");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get file info for blank ID."));

            throw e;
        }
    }

    @Test
    public void getTranslateFile_whenNullFileId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getTranslatedFile(null, "en-us");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get translated file info for null ID."));

            throw e;
        }
    }

    @Test
    public void getTranslatedFile_whenBlankFileId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getTranslatedFile(" ", "en-us");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get translated file info for blank ID."));

            throw e;
        }
    }

    @Test
    public void authorizeQuote_withNullQuoteId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.authorizeQuote(null, AUTOMATICALLY_APPROVED_PO_NUMBER);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot authorize quote for null ID."));

            throw e;
        }
    }

    @Test
    public void authorizeQuote_withBlankQuoteId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.authorizeQuote(" ", AUTOMATICALLY_APPROVED_PO_NUMBER);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot authorize quote for blank ID."));

            throw e;
        }
    }

    @Test
    public void authorizeQuote_withNullPoNumber_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.authorizeQuote("42", null);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot authorize quote for null PO number."));

            throw e;
        }
    }

    @Test
    public void authorizeQuote_withBlankPoNumber_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.authorizeQuote("42", " ");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot authorize quote for blank PO number."));

            throw e;
        }
    }

    @Test
    public void rejectTranslatedFile_withNullFileId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.rejectTranslatedFile(null, "en-us");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot reject translated file for null ID."));

            throw e;
        }
    }

    @Test
    public void rejectTranslatedFile_withBlankFileId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.rejectTranslatedFile(" ", "en-us");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot reject translated file for blank ID."));

            throw e;
        }
    }

    @Test
    public void getQuotesForJob_whenNullId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getQuotesForJob(null);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get quote info for null job ID."));

            throw e;
        }
    }

    @Test
    public void getQuotesForJob_whenBlankId_throwsExceptionWithMessage() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getQuotesForJob(" ");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get quote info for blank job ID."));

            throw e;
        }
    }

    @Test
    public void retrieveUnitCounts() throws ContentAPIException, InterruptedException {
        LBFile addedFile = api.addFile("text/plain", new File("src/test/resources/ContentAPI_addFile_test.txt"), "en-us");

        Project addedProject = api.addProject(
                "retrieveUnitCountTest",
                createTranslationOptions(),
                createFileAssetIds(addedFile)
        );

        Quote addedQuote = api.addQuote(
                createTranslationOptions(),
                createProjectList(addedProject)
        );

        Quote retrievedQuote = api.getQuote(valueOf(addedQuote.getQuoteId()));
        while(retrievedQuote.getStatus().equals("Calculating")) {
            Thread.sleep(1000);
            retrievedQuote = api.getQuote(valueOf(retrievedQuote.getQuoteId()));
        }

        Project retrievedProject = api.getProject(valueOf(addedProject.getProjectId()));

        assertThat(retrievedProject.getTargetLanguages().get(0).getUnits(), is(1));
    }

    @Test
    public void rejectQuote() throws ContentAPIException, InterruptedException {
        String projectName = getTimeStamp();

        LBFile addedFile = api.addFile("text/plain", new File("src/test/resources/ContentAPI_addFile_test.txt"), "en-us");
        LBFile addedReferenceFile = api.addFile("text/plain", new File("src/test/resources/ContentAPI_addFile_test2.txt"), "en-us");

        Project addedProject = api.addProject(
                projectName,
                createTranslationOptions(),
                createFileAssetIds(addedFile),
                createFileAssetIds(addedReferenceFile),
                addProjectNotifications
        );

        Quote quoteToReject = api.addQuote(
                createTranslationOptions(),
                createProjectList(addedProject),
                addQuoteNotifications
        );

        while(quoteToReject.getStatus().equals("Calculating")) {
            Thread.sleep(1000);
            quoteToReject = api.getQuote(valueOf(quoteToReject.getQuoteId()));
        }

        RejectQuote rejectedQuote = api.rejectQuote(valueOf(quoteToReject.getQuoteId()));

        assertThat(rejectedQuote.getStatus(), is("Success"));
    }

    @Test
    public void addFileByReference_withValidLanguageCode() throws ContentAPIException {
        URI uri = null;
        try {
            uri = new URI("https://bitbucket.org/liox-ondemand/liox-content-sdk-java/raw/3d1ea2817e1db258006f7039c0242bcc1f2aa4a4/LICENSE.txt");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        LBFile referencedFile = api.addFileByReference(uri, "en-us");
        assertThat(referencedFile.getAssetId(), notNullValue());
    }

    @Test
    public void addFileByReference_withNullLanguageCode_detectsLanguageCode() throws ContentAPIException {
        URI uri = null;
        try {
            uri = new URI("https://bitbucket.org/liox-ondemand/liox-content-sdk-java/raw/3d1ea2817e1db258006f7039c0242bcc1f2aa4a4/LICENSE.txt");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        LBFile referencedFile = api.addFileByReference(uri, null);
        assertThat(referencedFile.getAssetId(), notNullValue());
        assertThat(referencedFile.getSourceLanguage().getLanguageCode(), is(nullValue()));
    }

    @Test
    public void addQuote_withNullTranslationOptions_throwsException() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.addQuote(null, null, null);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Must specify options to generate a quote"));

            throw e;
        }
    }

    @Test
    @SuppressWarnings("deprecation")
    public void listLocales_returnsLocalesList() throws ContentAPIException {
        List<Locale> locales = api.listLocales().getLocales();

        assertThat(locales, Matchers.<Locale>hasItem(hasProperty("name", equalTo("English (United States)"))));
        assertThat(locales, Matchers.<Locale>hasItem(hasProperty("code", equalTo("en-us"))));
    }

    @Test
    public void getLocales_returnsListOfLocales() throws ContentAPIException {
        List<Locale> locales = api.getLocales();

        assertThat(locales, Matchers.<Locale>hasItem(hasProperty("name", equalTo("English (United States)"))));
        assertThat(locales, Matchers.<Locale>hasItem(hasProperty("code", equalTo("en-us"))));
    }

    @Test
    @SuppressWarnings("deprecation")
    public void listServices_returnsServiceList() throws ContentAPIException {
        List<Service> services = api.listServices().getServices();

        assertThat(services, Matchers.<Service>hasItem(hasProperty("serviceId", equalTo(PSEUDO_TRANSLATION_SERVICE_ID))));
    }

    @Test
    public void getServices_returnsListOfServices() throws ContentAPIException {
        List<Service> services = api.getServices();

        assertThat(services, Matchers.<Service>hasItem(hasProperty("serviceId", equalTo(PSEUDO_TRANSLATION_SERVICE_ID))));
    }

    @Test
    public void getServices_filteredByExtension_returnsFilteredListOfServices() throws ContentAPIException {
        List<Service> services = api.getServices("html");

        assertThat(services, Matchers.<Service>hasItem(hasProperty("serviceId", equalTo(MACHINE_TRANSLATION_SERVICE_ID))));
        assertThat(services, not(Matchers.<Service>hasItem(hasProperty("serviceId", equalTo(PSEUDO_TRANSLATION_SERVICE_ID)))));
        assertThat(services, not(Matchers.<Service>hasItem(hasProperty("serviceId", equalTo(PSEUDO_WPML_SERVICE_ID)))));

    }

    @Test
    public void getService_returnsServiceInfo() throws ContentAPIException {
        Service service = api.getService(PSEUDO_TRANSLATION_SERVICE_ID);

        assertThat(service.getName(), equalTo("Pseudo Translation"));
    }

    @Test
    public void addFile_returnsAnLBFile() throws ContentAPIException {
        LBFile file = api.addFile("text/xml", new File("src/test/resources/ContentAPI_addFile_html.xml"), "en-us");

        assertThat(file.getAssetId(), notNullValue());
        assertThat(file.getSourceLanguage().getLanguageCode(), equalTo("en-us"));
    }

    @Test
    public void addFile_withInvalidLocale_throwsException() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.addFile(
                    "text/plain",
                    new File("src/test/resources/ContentAPI_addFile_test.txt"),
                    "foo-bar"
            );
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("foo-bar"));
            assertThat(e.getErrors().size(), greaterThan(0));

            throw e;
        }
    }

    @Test
    public void listQuotes_returnsSuccessfully() throws ContentAPIException {
        List<Quote> quotes = api.listQuotes();

        assertThat(quotes.size(), greaterThan(0));
        assertThat(quotes.get(0).getQuoteId(), notNullValue());
    }

    @Test
    public void listProjects_returnsSuccessfully() throws ContentAPIException {
        List<Project> projects = api.listProjects();

        assertThat(projects.size(), greaterThan(0));
        assertThat(projects.get(0).getProjectId(), notNullValue());
    }

    @Test
    public void addFile_withZipFile() throws ContentAPIException {
        ZipUtils zipUtility = new ZipUtils("", testName.getMethodName());

        LBFile file = api.addFile(
                "application/zip",
                createZipFile(zipUtility),
                null
        );

        SourceLanguage sourceLanguage = file.getSourceLanguage();
        assertThat(sourceLanguage, instanceOf(SourceLanguage.class));

        String zipFilePath = api.getFile(Integer.toString(file.getAssetId()));
        assertNotNull(zipFilePath);

        zipUtility.cleanUpZipFileAndFolder(false);
    }

    @Test
    public void getFileDetails_withNullId_throwsContentApiException() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getFileDetails(null);
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get file details for null asset ID."));

            throw e;
        }
    }

    @Test
    public void getFileDetails_withBlankId_throwsContentApiException() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.getFileDetails("");
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Cannot get file details for blank asset ID."));

            throw e;
        }
    }

    @Test
    public void addProject_withoutReferenceFiles() throws Exception {
        String timestamp = getTimeStamp();

        ZipUtils zipUtility = new ZipUtils("", testName.getMethodName());

        LBFile addedFiles = api.addFile(
                "application/zip",
                createZipFile(zipUtility),
                new SourceLanguage("en-us").getLanguageCode()
        );

        Project project = api.addProject(timestamp, getOptions(getFirstService(false)), createFileAssetIds(addedFiles));

        assertEquals(project.getServiceId(), PSEUDO_TRANSLATION_SERVICE_ID);
        assertNotNull(project.getCreationDate());

        zipUtility.cleanUpZipFileAndFolder(true);
    }

    @Test
    public void addProject_withReferenceFiles() throws ContentAPIException {
        LBFile fileForTranslation = api.addFile("text/plain", new File("src/test/resources/ContentAPI_addFile_test.txt"), "en-us");
        assertThat(fileForTranslation.getAssetId(), notNullValue());

        LBFile fileForReference = api.addFile("text/plain", new File("src/test/resources/ContentAPI_addFile_test.txt"), "en-us");
        assertThat(fileForReference.getAssetId(), notNullValue());

        Project addedProject = api.addProject("Project with Reference Files", createTranslationOptions(), createFileAssetIds(fileForTranslation), createFileAssetIds(fileForReference), null);
        assertThat(addedProject.getName(), equalTo("Project with Reference Files"));
        assertThat(addedProject.getFiles().size(), equalTo(1));
        assertThat(addedProject.getReferenceFiles().size(), equalTo(1));
    }

    @Test
    public void addProject_withInvalidFiles() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        try {
            api.addProject(
                    "Project with Invalid File",
                    createTranslationOptions(),
                    Collections.singletonList("000")
            );
        } catch (ContentAPIException contentApiException) {
            assertThat(contentApiException.getMessage(), containsString("No File found"));
            assertThat(contentApiException.getErrors().size(), equalTo(1));

            throw contentApiException;
        }
    }

    @Test
    public void addQuote_withInvalidProject() throws ContentAPIException {
        thrown.expect(ContentAPIException.class);

        Project invalidProject = new Project();
        invalidProject.setProjectId(0);

        try {
            api.addQuote(
                    createTranslationOptions(),
                    createProjectList(invalidProject)
            );
        } catch (ContentAPIException e) {
            assertThat(e.getMessage(), containsString("Project does not exist.: A project with this ID 0 does not exist in the system."));

            throw e;
        }
    }

    @Test
    public void testGetTranslatedFile() throws ContentAPIException {
        String assetId = null;
        String languageCode = null;
        List<Quote> allQuotes = api.listQuotes();

        if (null != allQuotes) {
            if (!allQuotes.isEmpty()) {
                int quoteIndex = allQuotes.size() - 1;
                while (0 <= quoteIndex) {
                    Quote quote = allQuotes.get(quoteIndex);
                    if (!quote.toXmlString().contains(".zip")) {
                        for (Project project : quote.getProjects()) {
                            for (LBFile file : project.getFiles()) {
                                if (file.getFileStatus() == FileStatus.Translated) {
                                    assetId = Integer.toString(file.getAssetId());
                                    languageCode = project.getTargetLanguages().get(0).getLanguageCode();

                                    break;
                                }
                            }

                            if (null != assetId) {
                                // break project loop
                                break;
                            }
                        }

                        if (null != assetId) {
                            // break quote loop
                            break;
                        }

                    }

                    quoteIndex--;
                }
            }
        }

        // Set up constants in the event that listQuotes returns nothing.
        // Note that this will fail if no translations have completed recently.
        if (null == assetId) {
            assetId = "239863";
            languageCode = "fr-fr";
        }

        // if using the default file, just catch any exceptions and declare
        // the test a success. We don't want to fail the test because of not
        // having any translated files left in the system for this test account.
        try {
            api.getTranslatedFile(assetId, languageCode);
        } catch (Exception e1) {

        }
    }

    @Test
    @Ignore("This requires a manual entry for file asset ID and language code")
    public void testRejectFile() throws ContentAPIException {
        RejectTranslationOutput rejectOutput = api.rejectTranslatedFile("237854", "es-es");

        assertNotNull(rejectOutput);
    }

    @Test
    @Ignore("Does not work")
    public void testListProjects() {
        try {
            List<Project> projects = api.listProjects();
            assertNotNull(projects);
            if (!projects.isEmpty()) {
                Iterator<Project> projectIter = projects.iterator();
                if (projectIter.hasNext()) {
                    Project mostRecent = projectIter.next();
                    while (projectIter.hasNext()) {
                        mostRecent = projectIter.next();
                        if (!mostRecent.getStatus().toUpperCase().contains("NEW")) {
                            Project projectInfo = api.getProject(Integer.toString(mostRecent.getProjectId()));
                        }
                    }
                }
            } else {

            }
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    @Test
    @Ignore("Does not work")
    public void testGetQuotesForJob() {
        String jobName = "/var/lionbridge-ondemand-connector/20160502/multiquote_translationjob";

        try {
            List<Quote> jobQuotes = api.getQuotesForJob(jobName);
            assertNotNull(jobQuotes);
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    @Test
    @Ignore("Does not work")
    public void testGenerateQuote() throws Exception {
        Service service = getFirstService(false);

        Project project = createProject(service);
        List<Project> projects = createProjectList(project);

        Quote quote = api.addQuote(getOptions(service), projects);
        assertNotNull(quote.getQuoteId());

        Project projectInfo = api.getProject(Integer.toString(project.getProjectId()));
        assertNotNull(projectInfo.getName());

        QuoteAuthorization quoteAuth = api.authorizeQuote(Integer.toString(quote.getQuoteId()), AUTOMATICALLY_APPROVED_PO_NUMBER);
        assertNotNull(quoteAuth);

        Quote quoteInfo = api.getQuote(Integer.toString(quote.getQuoteId()));
        assertNotNull(quoteInfo.getQuoteId());
    }

    @Test
    @Ignore("Does not work")
    public void testGenerateZipQuote() throws Exception {
        Service service = getFirstService(false);
        ZipUtils zipUtility = new ZipUtils("", testName.getMethodName());
        Project project = createZipProject(service, zipUtility);

        List<Project> projects = createProjectList(project);

        Quote quote = api.addQuote(getOptions(service), projects);
        assertNotNull(quote.getQuoteId());

        zipUtility.cleanUpZipFileAndFolder(false);

        Project projectInfo = api.getProject(Integer.toString(project.getProjectId()));
        assertNotNull(projectInfo.getName());

        QuoteAuthorization quoteAuth = api.authorizeQuote(Integer.toString(quote.getQuoteId()), AUTOMATICALLY_APPROVED_PO_NUMBER);
        assertNotNull(quoteAuth);

        Quote quoteInfo = api.getQuote(Integer.toString(quote.getQuoteId()));
        assertNotNull(quoteInfo.getQuoteId());
    }

    @Test
    @Ignore("Does not work")
    public void testGenerateImageQuote() throws Exception {
        Service service = getFirstService(false);

        Project project = createImageProject();
        List<Project> projects = createProjectList(project);

        Quote quote = api.addQuote(getOptions(service), projects);
        assertNotNull(quote.getQuoteId());

        Project projectInfo = api.getProject(Integer.toString(project.getProjectId()));
        assertNotNull(projectInfo.getName());

        QuoteAuthorization quoteAuth = api.authorizeQuote(Integer.toString(quote.getQuoteId()), AUTOMATICALLY_APPROVED_PO_NUMBER);
        assertNotNull(quoteAuth);

        Quote quoteInfo = api.getQuote(Integer.toString(quote.getQuoteId()));
        assertNotNull(quoteInfo.getQuoteId());
    }

    private List<Project> createProjectList(Project addedProject) {
        List<Project> projectList = new ArrayList<>();
        projectList.add(addedProject);
        return projectList;
    }

    private List<String> createFileAssetIds(LBFile addedFile) {
        List<String> fileAssetIds = new ArrayList<>();
        fileAssetIds.add(valueOf(addedFile.getAssetId()));
        return fileAssetIds;
    }

    private TranslationOptions createTranslationOptions() {
        List<TargetLanguage> targetLanguages = new ArrayList<>();
        targetLanguages.add(new TargetLanguage("de-de"));

        TranslationOptions translationOptions = new TranslationOptions();
        translationOptions.setCurrency("USD");
        translationOptions.setSourceLanguage(new SourceLanguage("en-us"));
        translationOptions.setTargetLanguages(targetLanguages);
        translationOptions.setServiceId(PSEUDO_TRANSLATION_SERVICE_ID);
        translationOptions.setSpecialInstructions("Please don't translate the words Foo, Bar and Baz");

        return translationOptions;
    }

    @SuppressWarnings("deprecation")
    private Service getFirstService(boolean imageSearch) throws ContentAPIException {
        List<Service> services = api.listServices().getServices();

        Service firstService = null;

        for (Service service : services) {
            firstService = service;

            if (PSEUDO_TRANSLATION_SERVICE_ID == firstService.getServiceId() && !imageSearch) {
                break;
            } else if (239 == firstService.getServiceId() && imageSearch) {
                break;
            }
        }

        return firstService;
    }

    private TranslationOptions getOptions(Service service) {
        List<String> translationLanguages = new ArrayList<>();
        SourceLanguage sourceLanguage = new SourceLanguage("en-us");
        TargetLanguage targetLanguage = service.getTargetLanguages().get(service.getTargetLanguages().size() - 1);

        translationLanguages.add(targetLanguage.getLanguageCode());
        TranslationOptions options = new TranslationOptions(sourceLanguage.getLanguageCode(), translationLanguages);
        options.initialize(api, service);

        return options;
    }

    private Project createProject(Service service) throws ContentAPIException {
        LBFile file = postXmlFile(
                new SourceLanguage("en-us").getLanguageCode()
        );

        String fileAssetId = valueOf(file.getAssetId());
        List<String> fileList = new ArrayList<>();
        fileList.add(fileAssetId);

        //Execute addProject and check resulting attributes.
        Date curTime = new Date();
        String curTimeString = (new SimpleDateFormat("yyyyMMddHHmmss")).format(curTime);

        return api.addProject("TestProject." + curTimeString, getOptions(service), fileList);
    }

    private Project createZipProject(Service service, ZipUtils zipUtility) throws ContentAPIException {
        SourceLanguage sourceLanguage = new SourceLanguage("en-us");

        //Get file assetId string
        LBFile file = addZipFile(sourceLanguage.getLanguageCode(), zipUtility);
        String fileAssetId = valueOf(file.getAssetId());
        List<String> fileList = new ArrayList<>();
        fileList.add(fileAssetId);

        //Execute addProject and check resulting attributes.
        Date curTime = new Date();
        String curTimeString = (new SimpleDateFormat("yyyyMMddHHmmss")).format(curTime);

        return api.addProject("TestProject." + curTimeString, getOptions(service), fileList);
    }

    private Project createImageProject() throws ContentAPIException {
        SourceLanguage sourceLanguage = new SourceLanguage("en-us");

        //Get file assetId string
        LBFile file = postImageFile(sourceLanguage.getLanguageCode());
        String fileAssetId = valueOf(file.getAssetId());
        List<String> fileList = new ArrayList<>();
        fileList.add(fileAssetId);

        //Execute addProject and check resulting attributes.
        Date curTime = new Date();
        String curTimeString = (new SimpleDateFormat("yyyyMMddHHmmss")).format(curTime);
        Service service = getFirstService(true);

        return api.addProject("TestProjectJpeg." + curTimeString, getOptions(service), fileList);
    }

    private LBFile postXmlFile(String languageCode) throws ContentAPIException {
        File inputFile = new File("target/test-classes/ContentAPI_addFile_html.xml");

        return api.addFile("text/xml", inputFile, languageCode);
    }

    private LBFile addZipFile(String languageCode, ZipUtils zipUtility) throws ContentAPIException {
        File zipFile = createZipFile(zipUtility);

        return api.addFile("application/zip", zipFile, languageCode);
    }

    private LBFile postImageFile(String languageCode) throws ContentAPIException {
        File inputJpegFile = new File("target/test-classes/schrute.png");

        assertNotNull(inputJpegFile);

        return api.addFile("image/png", inputJpegFile, languageCode);
    }

    private File createZipFile(ZipUtils zipUtility) throws ContentAPIException {
        File inputXmlFile = new File("target/test-classes/ContentAPI_addFile_html.xml");
        assertNotNull(inputXmlFile);

        zipUtility.addFileToZipFolder(inputXmlFile);

        HashMap<String, List<String>> serviceMap = new HashMap<>();
        List<String> extensions = new ArrayList<>();
        extensions.add("XML");
        serviceMap.put("144", extensions);
        zipUtility.arrangeFilesByService(serviceMap);

        File zipFile = zipUtility.createZipFile("144");
        assertNotNull(zipFile);

        return zipFile;
    }

    private String getTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
    }
}
