package com.lionbridge.content.sdk;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class ErrorManager {
	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="Errors")
	private List<LBError> errors;

	@JacksonXmlProperty(localName="Status")
	private String status = "";

	private String errorCode="";

	public ContentAPIException generateException() {
		ContentAPIException exception;
		StringBuilder builder = new StringBuilder();

		LBError error = errors.get(0);

		if (error != null && error.getSimpleMessage() != null && !error.getSimpleMessage().isEmpty()) {
			if (error.getDetailedMessage() != null) {
				builder.append(error.getSimpleMessage()).append(": ").append(error.getDetailedMessage());
				exception = new ContentAPIException(builder.toString());
			} else{
				exception = new ContentAPIException(error.getSimpleMessage());
			}
		} else {
			exception = new ContentAPIException(this.errorCode);
		}

		exception.setErrors(errors);

		return exception;
	}

	public String toXmlString() {
		StringBuilder xmlString = new StringBuilder("\n<errors>");

		if(null != this.errors) {
			for(LBError currentError : this.errors) {
				xmlString.append("\n\t<error>");
				xmlString.append("\n\t\t<reasonCode>").append(currentError.getReasonCode()).append("</reasonCode>");
				xmlString.append("\n\t\t<simpleMessage>").append(currentError.getSimpleMessage()).append("</simpleMessage>");
				xmlString.append("\n\t\t<detailedMessage>").append(currentError.getDetailedMessage()).append("</detailedMessage>");
				xmlString.append("\n\t</error>");
			}
		}
		xmlString.append("\n</errors>");

		return xmlString.toString();
	}

	public List<LBError> getErrors() {
		return this.errors;
	}

	public void setErrors(final List<LBError> errors) {
		this.errors = errors;
	}

	public final String getStatus() {
		return this.status;
	}

	public final void setStatus(final String status) {
		this.status = status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(final String errorCode) {
		this.errorCode = errorCode;
	}
}
