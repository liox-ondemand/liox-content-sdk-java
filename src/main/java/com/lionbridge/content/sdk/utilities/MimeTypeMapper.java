package com.lionbridge.content.sdk.utilities;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class MimeTypeMapper {
    private static Map<String, String> filenameMimetypeMap = new HashMap<>();

    static {
        filenameMimetypeMap.put("json", "application/json");
        filenameMimetypeMap.put("resjson", "application/json");
        filenameMimetypeMap.put("pdf", "application/pdf");
        filenameMimetypeMap.put("epub", "application/epub+zip");
        filenameMimetypeMap.put("rtf", "application/rtf");
        filenameMimetypeMap.put("idml", "application/xml");
        filenameMimetypeMap.put("inx", "application/xml");
        filenameMimetypeMap.put("resw", "application/xml");
        filenameMimetypeMap.put("resx", "application/xml");
        filenameMimetypeMap.put("xlf", "application/xml");
        filenameMimetypeMap.put("xliff", "application/xml");
        filenameMimetypeMap.put("xml", "application/xml");
        filenameMimetypeMap.put("mif", "application/vnd.mif");
        filenameMimetypeMap.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        filenameMimetypeMap.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        filenameMimetypeMap.put("doc", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        filenameMimetypeMap.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        filenameMimetypeMap.put("odf", "application/vnd.oasis.opendocument.text");
        filenameMimetypeMap.put("odt", "application/vnd.oasis.opendocument.text");
        filenameMimetypeMap.put("ods", "application/vnd.oasis.opendocument.text");
        filenameMimetypeMap.put("ott", "application/vnd.oasis.opendocument.text");
        filenameMimetypeMap.put("zip", "application/zip");
        filenameMimetypeMap.put("jpg", "image/jpeg");
        filenameMimetypeMap.put("jpeg", "image/jpeg");
        filenameMimetypeMap.put("png", "image/png");
        filenameMimetypeMap.put("psd", "image/vnd.adobe.photoshop");
        filenameMimetypeMap.put("csv", "text/csv");
        filenameMimetypeMap.put("htm", "text/html");
        filenameMimetypeMap.put("html", "text/html");
        filenameMimetypeMap.put("ini", "text/plain");
        filenameMimetypeMap.put("po", "text/plain");
        filenameMimetypeMap.put("pot", "text/plain");
        filenameMimetypeMap.put("properties", "text/plain");
        filenameMimetypeMap.put("srt", "text/plain");
        filenameMimetypeMap.put("strings", "text/plain");
        filenameMimetypeMap.put("txt", "text/plain");
        filenameMimetypeMap.put("vtt", "text/plain");
        filenameMimetypeMap.put("yml", "text/yaml");
        filenameMimetypeMap.put("yaml", "text/yaml");
        filenameMimetypeMap.put("mp4", "video/mp4");
        filenameMimetypeMap.put("mov", "video/quicktime");
        filenameMimetypeMap.put("flv", "video/x-flv");
        filenameMimetypeMap.put("m4v", "video/x-m4v");
        filenameMimetypeMap.put("wmv", "video/x-ms-wmv");
        filenameMimetypeMap.put("mpeg", "video/mpeg");
    }

    /**
     * Returns the MIME Type for the given filename
     * @param filename The filename you want the MIME Type for
     * @return MIME Type as a string
     */
    public String getMimeType(String filename) {
        String[] pieces = filename.split("\\.");
        String extension = pieces[pieces.length - 1];

        if (filenameMimetypeMap.keySet().contains(extension)) {
            return filenameMimetypeMap.get(extension);
        } else {
            return null;
        }
    }

    /**
     * Returns the MIME Type for the given file
     * @param file The file you want the MIME Type for
     * @return MIME Type as a string
     */
    public String getMimeType(File file) {
        String filename = file.getName();

        return getMimeType(filename);
    }
}
