package com.lionbridge.content.sdk.utilities;

public final class XmlUtils {
    private XmlUtils() {}

    public static StringBuilder appendXmlTag(StringBuilder builder, String tagName, String value) {
        return builder.append("<").append(tagName).append(">")
                .append(value)
                .append("</").append(tagName).append(">");
    }
}
