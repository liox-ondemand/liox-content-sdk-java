package com.lionbridge.content.sdk;

import java.util.ArrayList;
import java.util.List;

public class ContentAPIException extends Exception {
	private List<LBError> errors = new ArrayList<>();

	public ContentAPIException() { }

	public ContentAPIException(final Throwable e) {
		super(e);
	}

	public ContentAPIException(final String message) {
		super(new Throwable(message));
	}

	public List<LBError> getErrors() {
		return errors;
	}

	public void setErrors(List<LBError> errors) {
		this.errors = errors;
	}
}
