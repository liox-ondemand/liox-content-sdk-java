package com.lionbridge.content.sdk.validations;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.lionbridge.content.sdk.definitions.FileStatus;
import com.lionbridge.content.sdk.models.LBFile;

import java.net.URI;
import java.text.SimpleDateFormat;

public class LBFileConverter extends StdConverter<LBFile, LBFile> {
	@Override
	public LBFile convert(LBFile original) {
		if(null != original.getFileStatusTemp()) {
            original.setFileStatus(FileStatus.valueOf(original.getFileStatusTemp().replaceAll("\\s+","")));
        }

		if (original.getName() == null || original.getName().isEmpty()) {
			original.setName(original.getFileName());
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); // Example: 2014-01-25T10:32:02Z

		try {
			if(original.getUploadDateString() != null && !original.getUploadDateString().isEmpty()) {
				original.setUploadDate(dateFormat.parse(original.getUploadDateString()));
			}
			if(original.getUrlString() != null && !original.getUrlString().isEmpty()) {
				original.setUrl(new URI(original.getUrlString()));
			}
		} catch (Exception e) { // Throws URISyntaxException, ParseException
			// Leave uploadDate blank. TODO: Is this proper form? If I throw, my converter will fail.
		}

		return original;
	}
}
