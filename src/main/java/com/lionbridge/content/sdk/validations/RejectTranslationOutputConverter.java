package com.lionbridge.content.sdk.validations;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.lionbridge.content.sdk.definitions.FileStatus;
import com.lionbridge.content.sdk.models.RejectTranslationOutput;

public class RejectTranslationOutputConverter extends StdConverter<RejectTranslationOutput, RejectTranslationOutput> {

	@Override
	public RejectTranslationOutput convert(RejectTranslationOutput original) {
		if (null != original.getFileStatusTemp()) {
			String originalFileStatusTemp = original.getFileStatusTemp().replaceAll("\\s+", "");
			original.setFileStatus(FileStatus.valueOf(originalFileStatusTemp));
		}

		return original;
	}

}
