package com.lionbridge.content.sdk.validations;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.lionbridge.content.sdk.models.ValidInputs;

public class ValidInputsConverter extends StdConverter<ValidInputs, ValidInputs> {
	@Override
	public ValidInputs convert(ValidInputs original) {
		original.setBooleans();
		return original;
	}
}
