package com.lionbridge.content.sdk.validations;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.lionbridge.content.sdk.models.Project;

import java.net.URI;

public class ProjectConverter extends StdConverter<Project, Project> {
	@Override
	public Project convert(Project original) {
		try {
			if (original.getProjectUrlString() != null && !original.getProjectUrlString().isEmpty()) {
				original.setProjectUrl(new URI(original.getProjectUrlString()));
			}
		} catch (Exception e) { // Throws URISyntaxException, ParseException
			// Leave projectUrl blank. TODO: Is this proper form? If I throw, my converter will fail.
		}
		return original;
	}
}
