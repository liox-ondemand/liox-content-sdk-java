package com.lionbridge.content.sdk.validations;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.lionbridge.content.sdk.definitions.TranslatedFileStatus;
import com.lionbridge.content.sdk.models.TargetLanguage;

import java.net.URI;
import java.net.URISyntaxException;

public class TargetLanguageConverter extends StdConverter<TargetLanguage, TargetLanguage> {
	@Override
	public TargetLanguage convert(TargetLanguage original) {
		if (null != original.getFileStatusTemp()) {
			original.setFileStatus(TranslatedFileStatus.valueOf(original.getFileStatusTemp().replaceAll("\\s+","")));
		}

		try {
			if (original.getProjectUrlString() != null && !original.getProjectUrlString().isEmpty()) {
				original.setProjectUrl(new URI(original.getProjectUrlString()));
			}
		} catch (URISyntaxException e) {
			// Leave projectUrl blank. TODO: Is this proper? If I throw the error, my converter will fail.
		}
		return original;
	}
}