package com.lionbridge.content.sdk.models.templates;

import com.lionbridge.content.sdk.models.LBFile;
import com.lionbridge.content.sdk.models.NotificationSubscription;
import com.lionbridge.content.sdk.models.Project;
import com.lionbridge.content.sdk.models.TranslationOptions;

import java.util.ArrayList;
import java.util.List;

import static com.lionbridge.content.sdk.utilities.XmlUtils.appendXmlTag;

public class GenerateQuoteTemplate {
	private TranslationOptions translationOptions = new TranslationOptions();
	private List<LBFile> files = new ArrayList<>();
	private List<LBFile> referenceFiles = new ArrayList<>();
	private List<Project> projects = new ArrayList<>();
    protected List<NotificationSubscription> notificationSubscriptions = new ArrayList<>();

    public GenerateQuoteTemplate(final List<String> fileAssetIds, final List<String> referenceAssetIds, TranslationOptions options) throws IllegalArgumentException {
		if (options == null) {
			throw new IllegalArgumentException("Options must be defined.");
		}
		this.addToFiles(LBFile.getFilesFromAssetIds(fileAssetIds));
		this.addToReferenceFiles(LBFile.getFilesFromAssetIds(referenceAssetIds));
		this.setTranslationOptions(options);
	}
	
	public GenerateQuoteTemplate(final TranslationOptions translationOptions, final List<Project> projects) {
        this(translationOptions, projects, null);
	}

	public GenerateQuoteTemplate(TranslationOptions translationOptions, List<Project> projects, List<NotificationSubscription> notificationSubscriptions) {
		this.translationOptions = translationOptions;
		this.addToProjects(projects);

		if (null != notificationSubscriptions) {
			this.addToNotifications(notificationSubscriptions);
		}
	}

    protected void addToNotifications(List<NotificationSubscription> notificationSubscriptions) {
        this.notificationSubscriptions.addAll(notificationSubscriptions);
    }

    public TranslationOptions getTranslationOptions() {
		return translationOptions;
	}

	public void setTranslationOptions(final TranslationOptions translationOptions) {
		this.translationOptions = translationOptions;
	}

	public List<LBFile> getFiles() {
		return files;
	}

	public void setFiles(final List<LBFile> files) {
		this.files = files;
	}

	public void addToFiles(final List<LBFile> files) {
		this.files.addAll(files);
	}

    public List<LBFile> getReferenceFiles() {
        return referenceFiles;
    }

    public void setReferenceFiles(List<LBFile> referenceFiles) {
        this.referenceFiles = referenceFiles;
    }

    public void addToReferenceFiles(final List<LBFile> files) {
		this.referenceFiles.addAll(files);
	}


	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(final List<Project> projects) {
		this.projects = projects;
	}

	public void addToProjects(final List<Project> projects) {
		this.projects.addAll(projects);
	}

	public String toXmlString() {
		StringBuilder builder = new StringBuilder();

		builder.append("<GenerateQuote>");

		if (hasFiles()) {
            appendXmlTag(builder, "Files", filesAsXml(getFiles(), false));
		}

		if (hasProjects()) {
			builder.append("<Projects>");
            for(Project project : getProjects()) {
                builder.append(project.idToXmlString());
            }
			builder.append("</Projects>");
		}

		builder = appendNofiticationsXml(builder);

		builder.append("</GenerateQuote>");

		return builder.toString();
	}

    protected StringBuilder appendNofiticationsXml(StringBuilder builder) {
		if (notificationSubscriptions.size() > 0) {
			builder.append("<NotificationSubscriptions>");
			for (NotificationSubscription notificationSubscription : notificationSubscriptions) {
				builder.append("<NotificationSubscription>");
                builder = appendXmlTag(builder, "EventName", notificationSubscription.getEventName());

				if (notificationSubscription.getEndpoint().startsWith("http")) {
					builder = appendXmlTag(builder, "Endpoint", notificationSubscription.getEndpoint());
				} else {
					builder = appendXmlTag(builder, "Endpoint", "mailto:" + notificationSubscription.getEndpoint());
				}
				builder.append("</NotificationSubscription>");
			}
			builder.append("</NotificationSubscriptions>");
		}
		return builder;
	}

    private boolean hasProjects() {
        return projects != null && projects.size() > 0;
    }

    protected boolean hasFiles() {
		return files != null && files.size() > 0;
	}

    protected boolean hasReferenceFiles() {
		return referenceFiles != null && referenceFiles.size() > 0;
	}

    protected String filesAsXml(List<LBFile> files, boolean areReferenceFiles) {
        StringBuilder builder = new StringBuilder();

        for (LBFile file: files) {
            builder.append(file.toXmlString(areReferenceFiles));
        }

        return builder.toString();
    }
}
