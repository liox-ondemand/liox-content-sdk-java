package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlRootElement;

@JsonDeserialize
@JacksonXmlRootElement(localName="Payment")
@XmlRootElement
public class Payment extends LBModel {
	@JacksonXmlProperty(localName="PaymentType")
	private String paymentType;

	@JacksonXmlProperty(localName="PaymentDescription")
	private String paymentDescription;

	@JacksonXmlProperty(localName="PaymentAmount")
	private String paymentAmount;

	@JacksonXmlProperty(localName="PaymentCurrency")
	private String paymentCurrency;

	public final String getPaymentType() {
		return this.paymentType;
	}

	public final void setPaymentType(final String paymentType) {
		this.paymentType = paymentType;
	}

	public final String getPaymentDescription() {
		return this.paymentDescription;
	}

	public final void setPaymentDescription(final String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public final String getPaymentAmount() {
		return this.paymentAmount;
	}

	public final void setPaymentAmount(final String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public final String getPaymentCurrency() {
		return this.paymentCurrency;
	}

	public final void setPaymentCurrency(final String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}
}
