package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.lionbridge.content.sdk.validations.ValidInputsConverter;

import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(converter=ValidInputsConverter.class)
public class ValidInputs {
	private Boolean acceptsFiles;
	private Boolean acceptsProducts;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="FileExtension")
	private List<String> fileExtensions = new ArrayList<>();

	@JacksonXmlProperty(localName="Files")
	private List<String> files = new ArrayList<>();

	@JacksonXmlProperty(localName="Products")
	private String products;

	public String printFiles() {
		StringBuilder debugString = new StringBuilder("ValidInputs={");
		debugString.append("acceptsFiles=").append(this.acceptsFiles.toString());

		// add file extensions
		if(null != this.fileExtensions) {
			if(!this.fileExtensions.isEmpty()) {
				debugString.append(", fileExtensions={");
				int fileExtCtr = 0;
				for(String fileExt:this.fileExtensions) {
					if(0 < fileExtCtr) {
						debugString.append(", ");
					}
					debugString.append(fileExt);
					fileExtCtr ++;
				}
				debugString.append("}");
			} else {
				debugString.append(", fileExtensions={}");
			}
		} else {
			debugString.append(", fileExtensions={NULL}");
		}

		// add files
		if(null != this.files) {
			if(!this.files.isEmpty()) {
				debugString.append(", files={");
				int fileCtr = 0;
				for(String file:this.files) {
					if(0 < fileCtr) {
						debugString.append(", ");
					}
					debugString.append(file);
					fileCtr ++;
				}
				debugString.append("}");
			} else {
				debugString.append(", files={}");
			}
		} else {
			debugString.append(", files={NULL}");
		}

		debugString.append("}");
		return debugString.toString();
	}

	/**
	 * Take an input file extension and compare against the valid extensions
	 * and files saved in this service. If any hits are found, return true.
	 * @param extensionIn extension to query
	 * @return true if a match is found in this service
	 */
	public boolean extensionValid(final String extensionIn) {

		// don't bother with this service if it doesn't accept files
		if(this.acceptsFiles) {
			// search files: these are where the extensions actually are
			if (null != this.files && !this.files.isEmpty()) {
				for (String file : this.files) {
					if (file.trim().equalsIgnoreCase(extensionIn.trim())) {
						return true;
					}
				}
			}

			// search file extensions: probably won't find a match here but try anyway if we get here
			if (null != this.fileExtensions && !this.fileExtensions.isEmpty()) {
				for (String fileExt : this.fileExtensions) {
					if (fileExt.trim().equalsIgnoreCase(extensionIn.trim())) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Count the number of files and extensions that match an extension in the input array.
	 * This method will be used to pick the best matching service if there are multiple
	 * matches for a particular type and there are multiple file types in the project.
	 * Ex. if the files supported are {DOCX,TXT,HTML,XML} and the input extensions are
	 * {CSV,TXT,XML}, this method should return 2 (for TXT and XML).
	 * @param activeExtensions list of file types to check
	 * @return number of items in the input list that match either a file or extension
	 */
	public int numberActiveExtensionsSupported(final List<String> activeExtensions) {
		int extensionCount = 0;

		// don't bother with this service if it doesn't accept files
		if(this.acceptsFiles) {
			// search files: these are where the extensions actually are
			if (null != this.files && !this.files.isEmpty()) {
				for (String extensionIn : activeExtensions) {
					for (String file : this.files) {
						if (file.trim().equalsIgnoreCase(extensionIn.trim())) {
							extensionCount++;
						}
					}
				}
			}

			// search file extensions: probably won't find a match here but try anyway if we get here
			if (null != this.fileExtensions && !this.fileExtensions.isEmpty()) {
				for (String extensionIn : activeExtensions) {
					for (String fileExt : this.fileExtensions) {
						if (fileExt.trim().equalsIgnoreCase(extensionIn.trim())) {
							extensionCount++;
						}
					}
				}
			}
		}

		return extensionCount;
	}

	// ONLY USE THIS FOR TESTING
	public boolean couldTryForExtension() {
        return acceptsFiles && !files.contains("xml") && !files.contains("XML");
    }

	public void setBooleans() {
		this.acceptsFiles = (getFiles().size() > 0);
		this.acceptsProducts = (products != null);
	}

	public Boolean getAcceptsProducts() {
		return this.acceptsProducts;
	}

	public void setAcceptsProducts(final Boolean acceptsProducts) {
		this.acceptsProducts = acceptsProducts;
	}

	public Boolean getAcceptsFiles() {
		return acceptsFiles;
	}

	public void setAcceptsFiles(final Boolean acceptsFiles) {
		this.acceptsFiles = acceptsFiles;
	}

	public List<String> getFileExtensions() {
		return this.fileExtensions;
	}

	public void setFileExtensions(final List<String> fileExtensions) {
		this.fileExtensions = fileExtensions;
	}

	public void addToFileExtensions(final String fileExtension) {
		this.fileExtensions.add(fileExtension);
	}

	public List<String> getFiles() {
		return this.files;
	}

	public void setFiles(final List<String> files) {
		this.files = files;
	}

	public String getProducts() {
		return this.products;
	}

	public void setProducts(final String products) {
		this.products = products;
	}
}
