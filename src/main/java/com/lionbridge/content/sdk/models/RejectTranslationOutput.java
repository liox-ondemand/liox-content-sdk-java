package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.lionbridge.content.sdk.definitions.FileStatus;
import com.lionbridge.content.sdk.validations.RejectTranslationOutputConverter;

import javax.xml.bind.annotation.XmlRootElement;

@JsonDeserialize(converter=RejectTranslationOutputConverter.class)
@JacksonXmlRootElement(localName="Files")
@XmlRootElement
public class RejectTranslationOutput extends LBModel {
	@JacksonXmlProperty(localName="AssetID")
	private int assetId;

	@JacksonXmlProperty(localName="Status")
	private String fileStatusTemp;

	private FileStatus fileStatus;

	@JacksonXmlProperty(localName="Name")
	private String name;

	@JacksonXmlProperty(localName="SourceLanguage")
	private SourceLanguage sourceLanguage;

	@JacksonXmlProperty(localName="TargetLanguage")
	private TargetLanguage targetLanguage;

	@JacksonXmlProperty(localName="Rejection")
	private RejectFile rejection;

	@JacksonXmlProperty(localName="Project")
	private Project project;

	public final int getAssetId() {
		return this.assetId;
	}
	public final void setAssetId(final int assetId) {
		this.assetId = assetId;
	}
	public final String getFileStatusTemp() {
		return this.fileStatusTemp;
	}
	public final void setFileStatusTemp(final String fileStatusTemp) {
		this.fileStatusTemp = fileStatusTemp;
	}
	public final FileStatus getFileStatus() {
		return this.fileStatus;
	}
	public final void setFileStatus(final FileStatus fileStatus) {
		this.fileStatus = fileStatus;
	}
	public final String getName() {
		return this.name;
	}
	public final void setName(final String name) {
		this.name = name;
	}
	public final SourceLanguage getSourceLanguage() {
		return this.sourceLanguage;
	}
	public final void setSourceLanguage(final SourceLanguage sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}
	public final TargetLanguage getTargetLanguage() {
		return this.targetLanguage;
	}
	public final void setTargetLanguage(final TargetLanguage targetLanguage) {
		this.targetLanguage = targetLanguage;
	}
	public final RejectFile getRejection() {
		return this.rejection;
	}
	public final void setRejection(final RejectFile rejection) {
		this.rejection = rejection;
	}
	public final Project getProject() {
		return this.project;
	}
	public final void setProject(final Project project) {
		this.project = project;
	}

}
