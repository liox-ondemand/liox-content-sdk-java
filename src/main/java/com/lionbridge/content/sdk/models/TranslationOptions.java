package com.lionbridge.content.sdk.models;

import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.utilities.XmlUtils;

import java.util.ArrayList;
import java.util.List;

public class TranslationOptions {
	private String currency;

	private int serviceId;

	private SourceLanguage sourceLanguage;

	private List<TargetLanguage> targetLanguages = new ArrayList<>();

	private String specialInstructions;

	public TranslationOptions() {}

	/**
	 * Creates TranslationOptions with a source language and target languages
	 * @param sourceLanguage A String representing the source language
	 * @param targetLanguages A list of Strings representing the desired target languages
	 */
	public TranslationOptions(String sourceLanguage, List<String> targetLanguages) {
		if (sourceLanguage == null) {
			throw new IllegalArgumentException("Must specify a source language.");
		}

		this.sourceLanguage = new SourceLanguage(sourceLanguage);

		if (targetLanguages != null) {
			for (String languageString : targetLanguages) {
				this.targetLanguages.add(new TargetLanguage(languageString));
			}
		}
	}

	public void initialize(ContentAPI client, Service service) {
		if (client == null) {
			throw new IllegalArgumentException("Client cannot be null");
		}

		if (service == null) {
			throw new IllegalArgumentException("Service cannot be null");
		}

		if (currency == null) {
			setCurrency(client.getDefaultCurrency());
		}

		setServiceId(service.getServiceId());

		List<TargetLanguage> targetLanguages = service.getTargetLanguages();
		if (targetLanguages == null) {
			setTargetLanguages(targetLanguages);
		}

		List<String> serviceSourceLanguageCodes = new ArrayList<>();
		List<SourceLanguage> sourceLanguages = service.getSourceLanguages();
		for (SourceLanguage language : sourceLanguages) {
			serviceSourceLanguageCodes.add(language.getLanguageCode());
		}

		if (!serviceSourceLanguageCodes.contains(sourceLanguage.getLanguageCode())) {
			throw new IllegalArgumentException("Source language must be in the Service's list of source languages");
		}

		if (targetLanguages.size() == 0) {
			throw new IllegalArgumentException("Must include at least one target language");
		}
		List<String> serviceTargetLanguageCodes = new ArrayList<>();
		for (TargetLanguage language : targetLanguages) {
			serviceTargetLanguageCodes.add(language.getLanguageCode());
		}
		for (TargetLanguage language : targetLanguages) {
			if (!serviceTargetLanguageCodes.contains(language.getLanguageCode())) {
				throw new IllegalArgumentException("Target languages must be in the Service's list of target languages");
			}
		}
    }

	public void initialize(Service service) {
		if (service == null) {
			throw new IllegalArgumentException("Service cannot be null");
		}
		this.setServiceId(service.getServiceId());
		List<TargetLanguage> targetLanguages = service.getTargetLanguages();
		if (this.targetLanguages == null) {
			this.setTargetLanguages(targetLanguages);
		}
		List<String> serviceSourceLanguageCodes = new ArrayList<>();
		for (int i=0; i<service.getSourceLanguages().size(); i++) {
			SourceLanguage language = service.getSourceLanguages().get(i);
			serviceSourceLanguageCodes.add(language.getLanguageCode());
		}
		if (!serviceSourceLanguageCodes.contains(this.sourceLanguage.getLanguageCode())) {
			throw new IllegalArgumentException("Source language must be in the Service's list of source languages");
		}
		if (this.targetLanguages.size() == 0) {
			throw new IllegalArgumentException("Must include at least one target language");
		}
		List<String> serviceTargetLanguageCodes = new ArrayList<>();
		for (TargetLanguage language : targetLanguages) {
			serviceTargetLanguageCodes.add(language.getLanguageCode());
		}
		for (TargetLanguage language : this.targetLanguages) {
			if (!serviceTargetLanguageCodes.contains(language.getLanguageCode())) {
				throw new IllegalArgumentException("Target lanauges must be in the Service's list of target languages");
			}
		}
	}

	public void initialize(ContentAPI client) {
		if (client == null) {
			throw new IllegalArgumentException("Client cannot be null");
		}

		if (currency == null) {
			setCurrency(client.getDefaultCurrency());
		}
	}

    /**
     * Set the Currency to use for the translation.
     *
     * @param currency String representing currency that the merchant wishes to pay with.
     */
	public void setCurrency(final String currency) {
		this.currency = currency;
	}

    public String getCurrency() {
        return currency;
    }

    /**
     * Numeric service code for the translation service. The service defines a set of options such as machine
     * translation with post edit on the title and raw machine translation on the body.
     *
     * @param serviceId The ID of the Service you wish to use
     */
	public void setServiceId(final int serviceId) {
		this.serviceId = serviceId;
	}

    public int getServiceId() {
        return serviceId;
    }

    /**
     * Sets the Source Language for the translation.
     *
     * @param sourceLanguage A locale code in the format en-us where EN is the 2 character ISO language code and US is
     *                       the 2 character ISO country code.
     */
	public void setSourceLanguage(final SourceLanguage sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

    public SourceLanguage getSourceLanguage() {
        return sourceLanguage;
    }

    /**
     * Sets the Target Languages for the translation.
     *
     * @param targetLanguages A list of locale codes in the format en-us where EN is the 2 character ISO language code
     *                        and US is the 2 character ISO country code.
     */
	public void setTargetLanguages(final List<TargetLanguage> targetLanguages) {
		this.targetLanguages = targetLanguages;
	}

    public List<TargetLanguage> getTargetLanguages() {
        return targetLanguages;
    }

    /**
     * Sets Special instructions for use by translators. This will be recorded on each project in the quote.
     *
     * @param specialInstructions Your special instructions as a String.
     */
	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

    public String toXmlString() {
        StringBuilder builder = new StringBuilder();
        builder.append("<TranslationOptions>");

        builder = XmlUtils.appendXmlTag(builder, "Currency", currency);

        if (serviceId != 0) {
            builder = XmlUtils.appendXmlTag(builder, "ServiceID", String.valueOf(serviceId));
        }

        if (sourceLanguage != null) {
            builder.append(sourceLanguage.toXmlString());
        }

        if (targetLanguages != null) {
            builder.append("<TargetLanguages>");
            for (TargetLanguage language : targetLanguages) {
                builder.append(language.toXmlString());
            }
            builder.append("</TargetLanguages>");
        }

        if (specialInstructions != null) {
            builder = XmlUtils.appendXmlTag(builder, "SpecialInstructions", specialInstructions);
        }

        builder.append("</TranslationOptions>");

        return builder.toString();
    }
}
