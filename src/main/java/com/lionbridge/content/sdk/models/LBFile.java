package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.lionbridge.content.sdk.definitions.FileStatus;
import com.lionbridge.content.sdk.validations.LBFileConverter;

import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.lang.String.format;

@JsonDeserialize(converter=LBFileConverter.class)
@JacksonXmlRootElement(localName="File")
@XmlRootElement
public class LBFile {
	@JacksonXmlProperty(localName="AssetID")
	private int assetId;

	@JacksonXmlProperty(localName="Status")
	private String fileStatusTemp;

	private FileStatus fileStatus;

	@JacksonXmlProperty(localName="URL")
	private String urlString;

	private URI url;

	@JacksonXmlProperty(localName="ProjectID")
	private int projectId;

	@JacksonXmlProperty(localName="Name")
	private String name;

	@JacksonXmlProperty(localName="FileName")
	private String fileName;

	@JacksonXmlProperty(localName="UploadDate")
	private String uploadDateString;

	private Date uploadDate;

	@JacksonXmlProperty(localName="SourceLanguage")
	private SourceLanguage sourceLanguage;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="TargetLanguages")
	private List<TargetLanguage> targetLanguages = new ArrayList<>();

	@JacksonXmlProperty(localName="AcceptedBy")
	private String acceptedBy;

    @JacksonXmlProperty(localName="AcceptedDate")
    private String acceptedDate;

    @JacksonXmlProperty(localName="AcceptedMethod")
    private String acceptedMethod;

	public String getUploadDateString() {
		return uploadDateString;
	}

	public void setUploadDateString(final String uploadDateString) {
		this.uploadDateString = uploadDateString;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(final Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public static String toXmlStringByUrl(URI url) {
		return format("<File><URL>%s</File></URL>", url.toString());
	}

	public String toXmlString() {
		return toXmlString(false);
	}

	public String toXmlString(boolean areReferenceFiles) {
		StringBuilder builder = new StringBuilder();

		if (areReferenceFiles) {
			builder.append("<ReferenceFile><AssetID>").append(String.valueOf(this.assetId)).append("</AssetID></ReferenceFile>");
		} else {
			builder.append("<File><AssetID>").append(String.valueOf(this.assetId)).append("</AssetID></File>");
		}

		return builder.toString();
	}
	
	public static List<LBFile> getFilesFromAssetIds(List<String> ids) throws NumberFormatException {
		List<LBFile> list = new ArrayList<>();
		if (ids != null) {
			for (String idString : ids) {
				LBFile file = new LBFile();
				int id = Integer.parseInt(idString);
				file.setAssetId(id);
				list.add(file);
			}
		}
		return list;
	}

	public URI getUrl() {
		return url;
	}

	public void setUrl(final URI url) {
		this.url = url;
	}

	public FileStatus getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(final FileStatus fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getFileStatusTemp() {
		return fileStatusTemp;
	}

	public void setFileStatusTemp(final String fileStatusTemp) {
		this.fileStatusTemp = fileStatusTemp;
	}

	public String getUrlString() {
		return urlString;
	}

	public void setUrlString(final String urlString) {
		this.urlString = urlString;
	}

	public int getAssetId() {
		return assetId;
	}

	public void setAssetId(final int assetId) {
		this.assetId = assetId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(final int projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	public SourceLanguage getSourceLanguage() {
		return sourceLanguage;
	}

	public void setSourceLanguage(final SourceLanguage sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	public List<TargetLanguage> getTargetLanguages() {
		return targetLanguages;
	}

	public void setTargetLanguages(final List<TargetLanguage> targetLanguages) {
		this.targetLanguages = targetLanguages;
	}

	public String getAcceptedBy() {
		return acceptedBy;
	}

    public String getAcceptedDate() {
        return acceptedDate;
    }

    public String getAcceptedMethod() {
        return acceptedMethod;
    }
}