package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName="SourceLanguage")
public class SourceLanguage extends LBLanguage {
	public SourceLanguage() {}

	public SourceLanguage(String languageCode) {
		this.setLanguageCode(languageCode);
	}

	public String toXmlString() {
		return "<SourceLanguage><LanguageCode>" + this.getLanguageCode() + "</LanguageCode></SourceLanguage>";
	}
}
