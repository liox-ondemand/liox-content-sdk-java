package com.lionbridge.content.sdk.models;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public abstract class LBModel {
    public String toXmlString() {
        String xmlString = "";
        try {
            JAXBContext context = JAXBContext.newInstance(this.getClass());
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // To format XML

            StringWriter sw = new StringWriter();
            m.marshal(this, sw);
            xmlString = sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return xmlString;
    }
}
