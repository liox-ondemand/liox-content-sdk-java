package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public abstract class LBLanguage {
	@JacksonXmlProperty(localName="LanguageCode")
	private String languageCode;

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		if (languageCode != null) {
			this.languageCode = languageCode.trim();
		}
	}

	public boolean equals(final LBLanguage other) {
		return other.getLanguageCode().trim().equalsIgnoreCase(getLanguageCode().trim());
	}
}
