package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

// TODO May need to handle ServiceCategory instead of ignoring it
@JsonIgnoreProperties({"UnitType", "MinimumUnits", "ServiceCategory"})
public class Service {
	@JacksonXmlProperty(localName="Name")
	private String name;

	@JacksonXmlProperty(localName="Description")
	private String description;

	@JacksonXmlProperty(localName="PriceDescription")
	private String priceDescription;

	@JacksonXmlProperty(localName="ServiceID")
	private int serviceId;

	@JacksonXmlProperty(localName="ValidInputs")
	private ValidInputs validInputs;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="SourceLanguages")
	private List<SourceLanguage> sourceLanguages = new ArrayList<>();

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="TargetLanguages")
	private List<TargetLanguage> targetLanguages = new ArrayList<>();

	/**
	 * Compare an input list of source languages against the list of allowable
	 * source languages for this service.
	 * @param sourceLanguagesIn input list of source languages to check
	 * @return true if all input source languages are valid for this service
	 */
	public boolean sourceLanguagesValid(final List<SourceLanguage> sourceLanguagesIn) {
		if(null == sourceLanguagesIn || null == this.sourceLanguages) {
			return false;
		}

		for(SourceLanguage srcLangIn : sourceLanguagesIn) {
			boolean sourceLanguageMatch = false;
			for(SourceLanguage serviceSourceLang : this.sourceLanguages) {
				if(serviceSourceLang.equals(srcLangIn)) {
					sourceLanguageMatch = true;
					break;
				}
			}

			if(!sourceLanguageMatch) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Compare an input list of target languages against the list of allowable
	 * target languages for this service.
	 * @param targetLanguagesIn input list of target languages to check
	 * @return true if all input target languages are valid for this service
	 */
	public boolean targetLanguagesValid(final List<TargetLanguage> targetLanguagesIn) {
		if(null == targetLanguagesIn || null == targetLanguages) {
			return false;
		}

		for(TargetLanguage tgtLangIn : targetLanguagesIn) {
			boolean targetLanguageMatch = false;
			for(TargetLanguage serviceTargetLang : targetLanguages) {
				if(serviceTargetLang.equals(tgtLangIn)) {
					targetLanguageMatch = true;
					break;
				}
			}

			if(!targetLanguageMatch) {
				return false;
			}
		}
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;

	}

	public String getPriceDescription() {
		return priceDescription;
	}

	public void setPriceDescription(final String priceDescription) {
		this.priceDescription = priceDescription;
	}

	public int getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(final int serviceId) {
		this.serviceId = serviceId;
	}

	public ValidInputs getValidInputs() {
		return validInputs;
	}

	public void setValidInputs(final ValidInputs validInputs) {
		this.validInputs = validInputs;
	}

	public List<TargetLanguage> getTargetLanguages() {
		return targetLanguages;
	}

	public void setTargetLanguages(final List<TargetLanguage> targetLanguages) {
		this.targetLanguages = targetLanguages;
	}

	public List<SourceLanguage> getSourceLanguages() {
		return sourceLanguages;
	}

	public void setSourceLanguages(final List<SourceLanguage> sourceLanguages) {
		this.sourceLanguages = sourceLanguages;
	}
}
