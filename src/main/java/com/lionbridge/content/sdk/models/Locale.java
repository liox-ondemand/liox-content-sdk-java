package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Locale {
    @JacksonXmlProperty(localName="Name")
    private String name;

    @JacksonXmlProperty(localName="Code")
    private String code;

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
