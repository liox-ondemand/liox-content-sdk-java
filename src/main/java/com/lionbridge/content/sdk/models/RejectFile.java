package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@JsonDeserialize
@JacksonXmlRootElement(localName="Rejection")
@XmlRootElement(name="RejectFile")
public class RejectFile extends LBModel {
	@JacksonXmlProperty(localName="ReasonCode")
	private int reasonCode;
	@JacksonXmlProperty(localName="ReasonDescription")
	private String reasonDescription;

	public RejectFile(final int reasonCodeIn, final String reasonDescriptionIn) {
		this.setReasonCode(reasonCodeIn);
		this.setReasonDescription(reasonDescriptionIn);
	}

	@XmlElement(name="ReasonCode")
	public final int getReasonCode() {
		return this.reasonCode;
	}

	public final void setReasonCode(final int reasonCode) {
		this.reasonCode = reasonCode;
	}
	
	@XmlElement(name="ReasonDescription")
	public final String getReasonDescription() {
		return this.reasonDescription;
	}
	
	public final void setReasonDescription(final String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

//	<RejectFile>
//	<ReasonCode>5000</ReasonCode>
//	<ReasonDescription>
//		Failed .po file validation
//
//		Failure Details:
//
//		<![CDATA[
//
//		#: od_api/exceptions.py:239 od_api/serializers.py:454
//msgid "Asset is already in use."
//msgstr "
//			^^^^^ expected end quote
//		]>
//
//</ReasonDescription>
//</RejectFile>

}
