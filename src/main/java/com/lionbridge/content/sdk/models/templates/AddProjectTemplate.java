package com.lionbridge.content.sdk.models.templates;

import com.lionbridge.content.sdk.models.NotificationSubscription;
import com.lionbridge.content.sdk.models.TranslationOptions;

import java.util.List;

import static com.lionbridge.content.sdk.utilities.XmlUtils.appendXmlTag;

public class AddProjectTemplate extends GenerateQuoteTemplate {
	private String projectName;

	public AddProjectTemplate(String projectName, TranslationOptions options, List<String> fileAssetIds, List<String> referenceFileAssetIds, List<NotificationSubscription> notificationSubscriptions) {
		super(fileAssetIds, referenceFileAssetIds, options);

        this.projectName = projectName;

        if (null != notificationSubscriptions) {
            this.addToNotifications(notificationSubscriptions);
        }
    }
	
	public String toXmlString() {
		StringBuilder builder = new StringBuilder();

		builder.append("<AddProject>");
		builder = appendXmlTag(builder, "ProjectName", projectName);
		builder.append(getTranslationOptions().toXmlString());

		if (hasFiles()) {
            appendXmlTag(builder, "Files", filesAsXml(getFiles(), false));
		}

		if (hasReferenceFiles()) {
            appendXmlTag(builder, "ReferenceFiles", filesAsXml(getReferenceFiles(), true));
        }

        builder = appendNofiticationsXml(builder);

        builder.append("</AddProject>");

		return builder.toString();
	}
}
