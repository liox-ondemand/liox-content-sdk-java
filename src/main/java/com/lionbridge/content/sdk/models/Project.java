package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.lionbridge.content.sdk.validations.ProjectConverter;

import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.Date;
import java.util.List;

import static java.lang.String.format;

@JsonDeserialize(converter=ProjectConverter.class)
@JacksonXmlRootElement(localName="Project")
@JsonIgnoreProperties({"Errors", "Products"})
@XmlRootElement
public class Project extends LBModel {
	@JacksonXmlProperty(localName="ProjectID")
	private int projectId;

	@JacksonXmlProperty(localName="ProjectName")
	private String name;

	@JacksonXmlProperty(localName="ServiceID")
	private int serviceId;

    @JacksonXmlProperty(localName="CreationDate")
    private Date CreationDate;

    @JacksonXmlProperty(localName="Price")
    private String price;

    @JacksonXmlProperty(localName="Currency")
    private String currency;

	@JacksonXmlProperty(localName="Status")
	private String status;

	@JacksonXmlProperty(localName="QuoteID")
	private int quoteId;

    @JacksonXmlProperty(localName="SpecialInstructions")
    private String specialInstructions;

	/**
	 * This URL value is returned from getProject and listProject
	 */
	@JacksonXmlProperty(localName="URL")
	private String url;
	
	/**
	 * ProjectURL is returned from getQuote
	 */
	@JacksonXmlProperty(localName="ProjectURL")
	private String projectUrlString;
	private URI projectUrl;

	/**
	 * This due date is returned from getQuote. using a string here since it's
	 * information only on this path, and the format can be different from other
	 * dates, which causes deserialization errors.
	 */
	@JacksonXmlProperty(localName="ProjectDueDate")
	private String projectDueDate;

	/**
	 * These two are returned from getProject and listProject
	 */
	@JacksonXmlProperty(localName="DueDate")
	private Date dueDate;

	@JacksonXmlProperty(localName="CompletionDate")
	private Date completionDate;

	@JacksonXmlProperty(localName="SourceLanguage")
	private SourceLanguage sourceLanguage; //NOTE: This was a string in original library.

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="TargetLanguages")
	private List<TargetLanguage> targetLanguages; //NOTE: This was a string in original library.

	// Deliberately leaving out products

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="Files")
	private List<LBFile> files;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="ReferenceFiles")
	private List<LBFile> referenceFiles;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="Errors")
	private List<String> errors;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="NotificationSubscriptions")
	private List<NotificationSubscription> notificationSubscriptions;

	/**
	 * Create an XML representation of this project but only include the ID.
	 * This is used for generating a quote.
	 * @return XML representation of the Project as a String
	 */
	public String idToXmlString() {
		return format("<Project><ProjectID>%d</ProjectID></Project>", this.getProjectId());
	}
	
	public String printTargetLanguages() {
		StringBuilder builder = new StringBuilder();
		builder.append("<TargetLanguages>");
		if (null != this.getTargetLanguages() && !this.getTargetLanguages().isEmpty()) {
			for (TargetLanguage tgtLang : this.getTargetLanguages()) {
				builder.append("<TargetLanguage>");
				builder.append("<LanguageCode>").append(tgtLang.getLanguageCode()).append("</LanguageCode>");
				builder.append("</TargetLanguage>");
			}
		}
		builder.append("</TargetLanguages>");
		return builder.toString();
	}

	public int getProjectId() {
		return this.projectId;
	}

	public void setProjectId(final int projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(final int serviceId) {
		this.serviceId = serviceId;
	}

	public URI getProjectUrl() {
		return this.projectUrl;
	}

	public void setProjectUrl(final URI projectUrl) {
		this.projectUrl = projectUrl;
	}

	public Date getCreationDate() {
		return this.CreationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.CreationDate = creationDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getProjectUrlString() {
		return this.projectUrlString;
	}

	public void setProjectUrlString(final String projectUrlString) {
		this.projectUrlString = projectUrlString;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(final String price) {
		this.price = price;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public String getProjectDueDate() {
		return this.projectDueDate;
	}

	public void setProjectDueDate(final String projectDueDate) {
		this.projectDueDate = projectDueDate;
	}

	public Date getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(final Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getCompletionDate() {
		return this.completionDate;
	}

	public void setCompletionDate(final Date completionDate) {
		this.completionDate = completionDate;
	}

	public SourceLanguage getSourceLanguage() {
		return this.sourceLanguage;
	}

	public void setSourceLanguage(final SourceLanguage sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	public List<TargetLanguage> getTargetLanguages() {
		return this.targetLanguages;
	}

	public void setTargetLanguages(final List<TargetLanguage> targetLanguages) {
		this.targetLanguages = targetLanguages;
	}

	public List<LBFile> getFiles() {
		return this.files;
	}

	public void setFiles(final List<LBFile> files) {
		this.files = files;
	}

	public List<String> getErrors() {
		return this.errors;
	}

	public void setErrors(final List<String> errors) {
		this.errors = errors;
	}

	public List<LBFile> getReferenceFiles() {
		return referenceFiles;
	}

	public void setReferenceFiles(List<LBFile> referenceFiles) {
		this.referenceFiles = referenceFiles;
	}

	public int getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(int quoteId) {
		this.quoteId = quoteId;
	}

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

	public List<NotificationSubscription> getNotificationSubscriptions() {
		return notificationSubscriptions;
	}

	public void setNotificationSubscriptions(List<NotificationSubscription> notificationSubscriptions) {
		this.notificationSubscriptions = notificationSubscriptions;
	}
}
