package com.lionbridge.content.sdk.models;

import java.util.List;

@Deprecated
/**
 * @deprecated This unused class will be removed in a future version. Use TranslationOptions instead.
 */
public class QuoteOptions extends TranslationOptions {
    @Deprecated
    /**
     * @deprecated This unused class will be removed in a future version. Use TranslationOptions instead.
     */
	public QuoteOptions(String sourceLanguage, List<String> targetLanguages, String currency) {
		super(sourceLanguage, targetLanguages);
		this.setCurrency(currency);
	}
}
