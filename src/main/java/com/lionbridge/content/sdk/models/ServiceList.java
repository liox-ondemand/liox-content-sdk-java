package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName="Services")
public class ServiceList {
	@JacksonXmlElementWrapper(useWrapping = false)
	@JacksonXmlProperty(localName="Service")
	private List<Service> services = new ArrayList<>();

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public List<Service> getValidServicesForExtensions(final List<String> extensions) {
		List<Service> validServices = new ArrayList<>();
		if (null != this.services && !services.isEmpty()) {
			for (Service service : services) {
				if (0 < service.getValidInputs().numberActiveExtensionsSupported(extensions)) {
					validServices.add(service);
				}
			}
		}
		return validServices;
	}
}
