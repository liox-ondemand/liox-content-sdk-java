package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.lionbridge.content.sdk.definitions.TranslatedFileStatus;
import com.lionbridge.content.sdk.validations.TargetLanguageConverter;

import java.net.URI;

import static java.lang.String.format;

@JsonDeserialize(converter=TargetLanguageConverter.class)
public class TargetLanguage extends LBLanguage {
	@JacksonXmlProperty(localName="Count")
	private int count;

	@JacksonXmlProperty(localName="Status")
	private String fileStatusTemp;

	private TranslatedFileStatus fileStatus;

	@JacksonXmlProperty(localName="ProjectURL")
	private String projectUrlString;

	private URI projectUrl;

	@JacksonXmlProperty(localName="DownloadURL")
	private String downloadUrlString;

	@JacksonXmlProperty(localName="URL")
	private String urlString;

	@JacksonXmlProperty(localName = "AcceptedBy")
	private String acceptedBy;

    @JacksonXmlProperty(localName = "AcceptedDate")
	private String acceptedDate;

    @JacksonXmlProperty(localName = "AcceptedMethod")
	private String acceptedMethod;

    @JacksonXmlProperty(localName = "Units")
    private Integer units;

    public TargetLanguage() {}

	public TargetLanguage(String languageCode) {
		this.setLanguageCode(languageCode);
	}

	public String toXmlString() {
		return format("<TargetLanguage><LanguageCode>%s</LanguageCode></TargetLanguage>", this.getLanguageCode());
	}

	public URI getProjectUrl() {
		return projectUrl;
	}

	public void setProjectUrl(final URI projectUrl) {
		this.projectUrl = projectUrl;
	}

	public TranslatedFileStatus getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(final TranslatedFileStatus fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getFileStatusTemp() {
		return fileStatusTemp;
	}

	public void setFileStatusTemp(final String fileStatusTemp) {
		this.fileStatusTemp = fileStatusTemp;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}

	public String getProjectUrlString() {
		return projectUrlString;
	}

	public void setProjectUrlString(final String projectUrlString) {
		this.projectUrlString = projectUrlString;
	}

	public String getDownloadUrlString() {
		return downloadUrlString;
	}

	public void setDownloadUrlString(final String downloadUrlString) {
		this.downloadUrlString = downloadUrlString;
	}

	public final String getUrlString() {
		return this.urlString;
	}

	public final void setUrlString(final String urlString) {
		this.urlString = urlString;
	}

    public String getAcceptedBy() {
        return acceptedBy;
    }

    public void setAcceptedBy(String acceptedBy) {
        this.acceptedBy = acceptedBy;
    }

    public String getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(String acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public String getAcceptedMethod() {
        return acceptedMethod;
    }

    public void setAcceptedMethod(String acceptedMethod) {
        this.acceptedMethod = acceptedMethod;
    }

	/**
	 * Returns the estimated number of words, minutes, or pages counted in the source file.
     * @return An Integer representing the count.
	 */
	public Integer getUnits() {
		return units;
	}

    public void setUnits(Integer units) {
        this.units = units;
    }
}

