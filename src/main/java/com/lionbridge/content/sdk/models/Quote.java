package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static com.lionbridge.content.sdk.utilities.XmlUtils.appendXmlTag;

@JsonDeserialize
@JacksonXmlRootElement(localName="Quote")
@XmlRootElement
public class Quote extends LBModel {
	@JacksonXmlProperty(localName="QuoteID")
	private int quoteId;

	@JacksonXmlProperty(localName="CreationDate")
	private Date creationDate;

	@JacksonXmlProperty(localName="TotalTranslations")
	private int totalTranslations;

	@JacksonXmlProperty(localName="TranslationCredit")
	private int translationCredit;

	@JacksonXmlProperty(localName="TranslationAcceptanceMethod")
	private String translationAcceptanceMethod;

	@JacksonXmlProperty(localName="Currency")
	private String currency;

	@JacksonXmlProperty(localName="TotalCost")
	private BigDecimal totalCost;

	@JacksonXmlProperty(localName="PrepaidCredit")
	private BigDecimal prepaidCredit;

	@JacksonXmlProperty(localName="AmountDue")
	private BigDecimal amountDue;

	@JacksonXmlProperty(localName="Status")
	private String status;

	@JacksonXmlProperty(localName="AuthorizeURL")
	private String authorizeUrl;

	@JacksonXmlProperty(localName="RejectURL")
	private String rejectUrl;

	@JacksonXmlProperty(localName="PaymentURL")
	private String paymentUrl;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="Projects")
	private List<Project> projects;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="Payments")
	private List<Payment> payments;

	private String internalBillingCode;

	private String purchaseOrderNumber;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="Errors")
	private List<String> errors;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="NotificationSubscriptions")
	private List<NotificationSubscription> notificationSubscriptions;

	public String toXmlForAuthorize() {
	    StringBuilder xmlString = new StringBuilder();

	    xmlString.append("<Quote>");
	    xmlString.append("<QuoteID>").append(getQuoteId()).append("</QuoteID>");
	    xmlString.append("<CreationDate>").append(getCreationDate().toString()).append("</CreationDate>");
	    xmlString.append("<TotalTranslations>").append(getTotalTranslations()).append("</TotalTranslations>");
	    xmlString.append("<TranslationCredit>").append(getTranslationCredit()).append("</TranslationCredit>");
	    xmlString.append("<TotalCost>").append(getTotalCost()).append("</TotalCost>");
	    xmlString.append("<PrepaidCredit>").append(getPrepaidCredit()).append("</PrepaidCredit>");
	    xmlString.append("<AmountDue>").append(getAmountDue()).append("</AmountDue>");
	    xmlString.append("<Currency>").append(getCurrency()).append("</Currency>");
	    xmlString.append("<PurchaseOrderNumber>").append(getPurchaseOrderNumber()).append("</PurchaseOrderNumber>");
	    xmlString.append("<Projects>");

	    if(null != getProjects()) {
	    	if(!getProjects().isEmpty()) {
	    		for(Project project : getProjects()) {
	    		    xmlString.append("<Project>");
	    		    xmlString.append("<ProjectID>").append(project.getProjectId()).append("</ProjectID>");
	    		    xmlString.append("<ProjectName>").append(project.getName()).append("</ProjectName>");
	    		    xmlString.append("<ServiceID>").append(project.getServiceId()).append("</ServiceID>");
	    		    xmlString.append("<SourceLanguage>");
	    		    xmlString.append("<LanguageCode>").append(project.getSourceLanguage().getLanguageCode()).append("</LanguageCode>");
	    		    xmlString.append("</SourceLanguage>");
	    		    xmlString.append("<TargetLanguages>");
					if (null != project.getTargetLanguages() && !project.getTargetLanguages().isEmpty()) {
						for (TargetLanguage tgtLang : project.getTargetLanguages()) {
							xmlString.append("<TargetLanguage>");
							xmlString.append("<LanguageCode>").append(tgtLang.getLanguageCode()).append("</LanguageCode>");
							xmlString.append("</TargetLanguage>");
						}
					}
	    		    xmlString.append("</TargetLanguages>");
	    		    xmlString.append("</Project>");

	    		}
	    	} else {
	    		return null;
	    	}
	    } else {
	    	return null;
	    }
	    xmlString.append("</Projects>");

	    if (null != notificationSubscriptions && notificationSubscriptions.size() > 0) {
			xmlString.append("<NotificationSubscriptions>");
			for (NotificationSubscription notification : notificationSubscriptions) {
				xmlString.append("<NotificationSubscription>");
				xmlString = appendXmlTag(xmlString, "EventName", notification.getEventName());
				if (notification.getEndpoint().startsWith("http")) {
					xmlString = appendXmlTag(xmlString, "Endpoint", notification.getEndpoint());
				} else {
					xmlString = appendXmlTag(xmlString, "Endpoint", "mailto:" + notification.getEndpoint());
				}
				xmlString.append("</NotificationSubscription>");
			}
			xmlString.append("</NotificationSubscriptions>");
		}

	    xmlString.append("</Quote>");
	    return xmlString.toString();
	}

	public int getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(final int quoteId) {
		this.quoteId = quoteId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getTotalTranslations() {
		return totalTranslations;
	}

	public void setTotalTranslations(final int totalTranslations) {
		this.totalTranslations = totalTranslations;
	}

	public int getTranslationCredit() {
		return translationCredit;
	}

	public void setTranslationCredit(final int translationCredit) {
		this.translationCredit = translationCredit;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(final BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getPrepaidCredit() {
		return prepaidCredit;
	}

	public void setPrepaidCredit(final BigDecimal prepaidCredit) {
		this.prepaidCredit = prepaidCredit;
	}

	public BigDecimal getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(final BigDecimal amountDue) {
		this.amountDue = amountDue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getAuthorizeUrl() {
		return authorizeUrl;
	}

	public void setAuthorizeUrl(final String authorizeUrl) {
		this.authorizeUrl = authorizeUrl;
	}

	public String getRejectUrl() {
		return rejectUrl;
	}

	public void setRejectUrl(final String rejectUrl) {
		this.rejectUrl = rejectUrl;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(final String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(final List<Project> projects) {
		this.projects = projects;
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(final List<Payment> payments) {
		this.payments = payments;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(final String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getInternalBillingCode() {
		return internalBillingCode;
	}

	public void setInternalBillingCode(final String internalBillingCode) {
		this.internalBillingCode = internalBillingCode;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(final List<String> errors) {
		this.errors = errors;
	}

    public String getTranslationAcceptanceMethod() {
        return translationAcceptanceMethod;
    }

    public void setTranslationAcceptanceMethod(String translationAcceptanceMethod) {
        this.translationAcceptanceMethod = translationAcceptanceMethod;
    }

	public List<NotificationSubscription> getNotificationSubscriptions() {
		return notificationSubscriptions;
	}

	public void setNotificationSubscriptions(List<NotificationSubscription> notificationSubscriptions) {
		this.notificationSubscriptions = notificationSubscriptions;
	}
}
