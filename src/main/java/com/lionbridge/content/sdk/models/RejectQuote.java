package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlRootElement;

@JacksonXmlRootElement(localName="RejectQuote")
@XmlRootElement
public class RejectQuote {
    @JacksonXmlProperty(localName="status")
    private String status;

    @JacksonXmlProperty(localName="error")
    private String error;

    public String getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setError(String error) {
        this.error = error;
    }
}
