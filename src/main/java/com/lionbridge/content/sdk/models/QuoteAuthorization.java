package com.lionbridge.content.sdk.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@JsonDeserialize
@JacksonXmlRootElement(localName="QuoteAuthorization")
@XmlRootElement
public class QuoteAuthorization extends LBModel {
	@JacksonXmlProperty(localName="Status")
	private String status;

	@JacksonXmlProperty(localName="QuoteURL")
	private String quoteUrl;

	@JacksonXmlElementWrapper
	@JacksonXmlProperty(localName="Projects")
	private List<AuthorizedProject> projects;

	@JacksonXmlProperty(localName="TranslationAcceptanceMethod")
	private String translationAcceptanceMethod;

	/**
	 * Note: all of these local classes have to be static in order to be deserialized using Jackson.
	 */
	@JsonDeserialize(converter=AuthorizedFileConverter.class)
	@JacksonXmlRootElement(localName="File")
	@XmlRootElement
	public static class AuthorizedFile {
		@JacksonXmlProperty(localName="Status")
		private String status;
		@JacksonXmlProperty(localName="AssetID")
		private int assetId;
		@JacksonXmlProperty(localName="FileName")
		private String fileName;

		public final String getStatus() {
			return this.status;
		}
		public final void setStatus(final String status) {
			this.status = status;
		}
		public final int getAssetId() {
			return this.assetId;
		}
		public final void setAssetId(final int assetId) {
			this.assetId = assetId;
		}
		public final String getFileName() {
			return this.fileName;
		}
		public final void setFileName(final String fileName) {
			this.fileName = fileName;
		}
	}

	public static class AuthorizedFileConverter extends StdConverter<AuthorizedFile, AuthorizedFile> {
		@Override
		public AuthorizedFile convert(AuthorizedFile original) {
			return original;
		}
    }

	@JsonDeserialize(converter=AuthorizedProjectConverter.class)
	@JacksonXmlRootElement(localName="Project")
	@XmlRootElement
	public static class AuthorizedProject {
		@JacksonXmlProperty(localName="ProjectID")
		private int projectId;
		@JacksonXmlProperty(localName="ProjectURL")
		private String projectUrl;
		@JacksonXmlProperty(localName="ProjectDueDate")
		private String projectDueDate;

		@JacksonXmlElementWrapper
		@JacksonXmlProperty(localName="Files")
		private List<AuthorizedFile> files;

        @JacksonXmlProperty(localName="SpecialInstructions")
        private String specialInstructions;

		public final int getProjectId() {
			return this.projectId;
		}

		public final void setProjectId(final int projectId) {
			this.projectId = projectId;
		}

		public final String getProjectUrl() {
			return this.projectUrl;
		}

		public final void setProjectUrl(final String projectUrl) {
			this.projectUrl = projectUrl;
		}

		public final String getProjectDueDate() {
			return this.projectDueDate;
		}

		public final void setProjectDueDate(final String projectDueDate) {
			this.projectDueDate = projectDueDate;
		}

		public final List<AuthorizedFile> getFiles() {
			return this.files;
		}

		public final void setFiles(final List<AuthorizedFile> files) {
			this.files = files;
		}

        public String getSpecialInstructions() {
            return specialInstructions;
        }

        public void setSpecialInstructions(String specialInstructions) {
            this.specialInstructions = specialInstructions;
        }
    }

	public static class AuthorizedProjectConverter extends StdConverter<AuthorizedProject, AuthorizedProject> {
		@Override
		public AuthorizedProject convert(AuthorizedProject original) {
			return original;
		}
    }

	public final String getStatus() {
		return this.status;
	}
	public final void setStatus(final String status) {
		this.status = status;
	}
	public final String getQuoteUrl() {
		return this.quoteUrl;
	}
	public final void setQuoteUrl(final String quoteUrl) {
		this.quoteUrl = quoteUrl;
	}
	public final List<AuthorizedProject> getProjects() {
		return this.projects;
	}
	public final void setProjects(final List<AuthorizedProject> projects) {
		this.projects = projects;
	}

	public String getTranslationAcceptanceMethod() {
		return translationAcceptanceMethod;
	}

	public void setTranslationAcceptanceMethod(String translationAcceptanceMethod) {
		this.translationAcceptanceMethod = translationAcceptanceMethod;
	}
}
