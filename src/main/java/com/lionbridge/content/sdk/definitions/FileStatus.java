package com.lionbridge.content.sdk.definitions;

public enum FileStatus {
	None(0), Analyzing(1), Analyzed(2), AnalysisFailed(3), InTranslation(4), Translated(5), New(6), Rejected(7), Referenced(8);

	private int value;

	FileStatus(final int value) {
		this.setValue(value);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
