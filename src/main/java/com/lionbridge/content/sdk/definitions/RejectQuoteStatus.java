package com.lionbridge.content.sdk.definitions;

public enum RejectQuoteStatus {
	Success(200), BadRequest(400), Unauthorized(401), Conflict(409), CommunicationError(0), Unknown(-1);

	private int value;

	RejectQuoteStatus(int value) {
		this.setValue(value);
	}

	public int getValue() {
		return value;
	}

	public void setValue(final int value) {
		this.value = value;
	}
}
