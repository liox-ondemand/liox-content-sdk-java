package com.lionbridge.content.sdk.definitions;

public enum LBContentType {
	csv("text/csv"),
	docx("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
	flv("video/x-flv"),
	htm("text/html"),
	html("text/html"),
	idml("application/xml"),
	ini("text/plain"),
	inx("application/xml"),
	jpg("image/jpeg"),
	json("application/json"),
	m4v("video/x-m4v"),
	mif("application/vnd.mif"),
	mov("video/quicktime"),
	mp4("video/mp4"),
	pdf("application/pdf"),
	png("image/png"),
	po("text/plain"),
	pptx("application/vnd.openxmlformats-officedocument.presentationml.presentation"),
	properties("text/plain"),
	psd("image/vnd.adobe.photoshop"),
	resjson("application/json"),
	resw("application/xml"),
	resx("application/xml"),
	rtf("application/rtf"),
	srt("text/plain"),
	strings("text/plain"),
	txt("text/plain"),
	vtt("text/plain"),
	wmv("video/x-ms-wmv"),
	xlf("application/xml"),
	xliff("application/xml"),
	xlsx("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
	XML("text/xml"),
	yml("text/yaml"),
	yaml("text/yaml"),
	zzzz("default");

	private String value;

	LBContentType(final String valueIn) {
		this.value = valueIn;
	}

	/**
	 * Count the number of times this value is used in the enum definition
	 * @return the number of matches
	 */
	private int countThisValue() {
		int valCount = 0;
		for (LBContentType contentType:LBContentType.values()) {
			if(contentType.getValue().equalsIgnoreCase(this.value)) {
				valCount ++;
			}
		}

		return (0 == valCount) ? 1 : valCount;
	}

	/**
	 * Pick the default return value in the event of multiple matches.
	 * @return true for the selected defaults or if there is only one match.
	 */
	public boolean isDefault() {
        return this == LBContentType.txt
                || this == LBContentType.XML
                || this == LBContentType.yaml
                || this == LBContentType.xlf
                || this == LBContentType.html
                || this == LBContentType.json
                || 1 == this.countThisValue();
    }

	/**
	 * Assign the most likely enumeration from the input string.
	 * If there are multiple matches
	 * @param valueIn enum value string to compare
	 * @return the best matching enum object
	 */
	public static LBContentType fromValue(final String valueIn) {
		for (LBContentType contentType:LBContentType.values()) {
            if (contentType.getValue().equalsIgnoreCase(valueIn) && contentType.isDefault()) {
                return contentType;
            }
		}
		return LBContentType.zzzz;
	}

	public String getValue() {
		return this.value;
	}

}
