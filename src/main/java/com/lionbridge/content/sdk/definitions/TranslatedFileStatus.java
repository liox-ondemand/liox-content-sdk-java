package com.lionbridge.content.sdk.definitions;

public enum TranslatedFileStatus {
	None(0), Started(1), Complete(2), Authorized(3), Referenced(4), Analyzed(5), Translated(6), InTranslation(7), Accepted(8);

	private int value;

	TranslatedFileStatus(final int value) {
		this.setValue(value);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
