package com.lionbridge.content.sdk;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class LBError {
	@JacksonXmlProperty(localName="ReasonCode")
	private int reasonCode;
	@JacksonXmlProperty(localName="SimpleMessage")
	private String simpleMessage;
	@JacksonXmlProperty(localName="DetailedMessage")
	private String detailedMessage;

	public String getDetailedMessage() {
		return detailedMessage;
	}

	public void setDetailedMessage(final String detailedMessage) {
		this.detailedMessage = detailedMessage;
	}

	public String getSimpleMessage() {
		return simpleMessage;
	}

	public void setSimpleMessage(final String simpleMessage) {
		this.simpleMessage = simpleMessage;
	}

	public int getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
}
