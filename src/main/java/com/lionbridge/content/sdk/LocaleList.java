package com.lionbridge.content.sdk;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.lionbridge.content.sdk.models.Locale;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName="Locales")
public class LocaleList {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName="Locale")
    private List<Locale> locales = new ArrayList<>();

    public List<Locale> getLocales() {
        return locales;
    }
}
