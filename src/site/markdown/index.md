# Lionbridge Content SDK for Java

## Prerequisites

In order to use the API connector you must first [sign up for a developer's account on the Lionbridge Sandbox](https://developer-sandbox.liondemand.com/accounts/register/).

You will then need to use the sandbox endpoint of `https://developer-sandbox.liondemand.com/api` (or your site specific endpoint if you have an enterprise onDemand account) and retrieve your Account Key Id and Account Secret Key.

## Getting Started

First add the onDemand Client as a dependency in your `pom.xml`:

```xml
<dependency>
    <groupId>com.lionbridge.content.sdk</groupId>
    <artifactId>liox-content-sdk-java</artifactId>
    <version>2.2.0</version>
</dependency>
```

## Initialize and Use the ContentAPI

To use the Content API:

- instantiate an instance using your access key and secret
- use the ContentAPI instance to send/received information
- display the results of your operation

Here is a quick example that lists the services available:

```java
public class ListServices {
    public static void main(String[] args) {
        private ContentAPI contentApi;
        	
        try {
            contentApi = new ContentAPI(
                    "LIOX_API_ACCESS_KEY_ID",
                    "LIOX_API_SECRET_KEY",
                    "LIOX_API_ENDPOINT",
                    "LIOX_API_DEFAULT_CURRENCY"
            );
        	
            ServiceList serviceList = contentApi.listServices();
            serviceList.getServices().stream().map(service -> service.getName()).forEach(System.out::println);

        } catch (ContentAPIException e) {
            // Deal with exception...
        }
    }
}
```

Check out [the full list of ContentAPI methods](apidocs/com/lionbridge/content/sdk/ContentAPI.html) to see what else is possible.

## Using the Snapshot Version

If you'd like to use the in-development version of the onDemand Client, you can use the snapshot version.

First add the Sonatype snapshot repository to your `pom.xml`:

```xml
<repositories>
    <repository>
        <id>oss-sonatype</id>
        <name>oss-sonatype</name>
        <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
```

Then add the snapshot dependency:

```xml
<dependency>
    <groupId>com.lionbridge.content.sdk</groupId>
    <artifactId>liox-content-sdk-java</artifactId>
    <version>2.3.0-SNAPSHOT</version>
</dependency>
```
