# Release Notes

## 2.2.0

- `TargetLanguage` now has a `Unit` field/method representing the estimated number of words, minutes, or pages counted in the source file.

## 2.1.0

- `addProject` now takes a list of `NotificationSubscription` objects instead of Strings
- `addQuote` now takes a list of `NotificationSubscription` objects instead of Strings
- Deprecates `authorizeQuote` with notification argument - this is not a feature of the API

## 2.0.3

- Adds `addFileByReference` method for adding files by URL
- Adds `rejectQuote` method for rejecting quotes
- `authorizeQuote` can now take notification endpoints
- Improves JavaDoc for `TranslationOptions`
- Deprecates legacy methods in `TranslationOptions`

## 2.0.2

- Ensure that ContentAPIExceptions have their errors set.
- TranslationOptions can now have SpecialInstructions
- Gracefully handle unknown XML properties
- Adds `getServices` which replaces/deprecates `listServices`
- Adds `getLocales` which replaces/deprecates `listLocales`.
- Deprecates `getQuotesForJob` with unused argument
- Deprecates `setUriDefaults`
- Deprecates `setDefaultCurrency`

## 2.0.1

- Project now requires Java 1.7
- Addresses missing/incorrect/incomplete JavaDocs

## 2.0.0

- New [end-user documentation](index.html) to get you up and running quickly with the JSDK
- Project is now published to Maven Central
- Moved project to a new `groupId`, `artifactId` and package.
- Updated to support the 2016-03-15 REST API
- Adds `listLocales` and associated models
- Adds `getService` method for getting a single service
- Adds `getFileDetails`
- Adds `isValid` method for checking if endpoint and credentials are valid
- Adds a `MimeTypeMapper` utility class for getting a file/filename's MIME type
- `addQuote` and `addProject` can now take a list of notification endpoints
- `addProject` now also accepts a list of reference files
- `ContentAPIException` now includes the API errors that caused the exception
- Changed `listProjects`, `listQuotes` and `getQuotesForJob` to return a `List` instead of implementation-specific `ArrayList` for more flexibility
- Changed `addProject` and `addQuote` to take a `List` of IDs instead of an implementation-specific `ArrayList` for more flexibility
- Changed `listProject` and `getProject` now return `Project` instead of `ProjectInfoOneTargetLanguage`
- No longer use `System.out` to log information
- No longer needlessly double-wrapping `ContentAPIExceptions`
- Removed the following unused classes:
    - `ProjectInfoOneTargetLanguage`
- Removed the following deprecated methods:
    - `Quote getQuoteForJob(final String jobId, final String objectTitle)`
    - `String getTranslatedFileName(final String assetId, final String languageCode)`
